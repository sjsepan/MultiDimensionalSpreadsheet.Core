using System;
// using System.Windows.Forms;
using Ssepan.Application.Core;
// 
using Ssepan.Collections.Core;
using Ssepan.Utility.Core;
// using System.Diagnostics;
using System.Reflection;

namespace MultiDimensionalSpreadsheetLibrary.Core.MVC
{
	/// <summary>
	/// This is the MVC Controller
	/// </summary>
	public class MDSSController<TModel> :
        ModelController<TModel>
        where TModel :
            class,
            IModel,
            new()
    {
        #region Constructors
        public MDSSController()
        {
        }
        #endregion Constructors

        #region Properties
        //Note:TModel Model exists in base
        #endregion Properties

        #region Methods

        /// <summary>
        /// Select a category item for a filter category.
        /// </summary>
        /// <param name="sheet">Sheet</param>
        /// <param name="categoryName">string</param>
        /// <returns>bool</returns>
        public static bool SelectFilterCategoryItem(Sheet sheet, string categoryName)
        {
            bool returnValue = default;
			try
			{
				//User must select a Category Item.
				CategoryItem categoryItem = SelectCategoryItem(sheet, categoryName, "Select a Category Item to Filter Sheet Cells.", -1, true);

				if (categoryItem != null)
                {
                    _ValueChanging = true;

                    //move it
                    sheet.SetFilterCategoryItem(categoryItem);

                    _ValueChanging = false;

                    //refresh
                    //Refresh();

                    returnValue = true;
                }
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);

                _ValueChanging = false;
                throw;
            }
            return returnValue;
        }

        /// <summary>
        /// Search until end or until another category with same sheet-category-type found; swap in latter case.
        /// </summary>
        /// <param name="sheet">Sheet</>
        /// <param name="categoryName">string</param>
        /// <param name="shiftType">ListOfTExtension.ShiftTypes</param>
        /// <returns>bool</returns>
        public static bool ShiftSheetCategory(Sheet sheet, string categoryName, ListOfTExtension.ShiftTypes shiftType)
        {
            bool returnValue = default;

            try
            {
                _ValueChanging = true;

                sheet.Categories.ShiftListItem<OrderedEquatableBindingList<Category>, Category>
                    (
                        shiftType,
                        item => item.Name == categoryName, //match on Category Name property
                        (item, itemSwap) => item.CategoryType == itemSwap.CategoryType //match on item Category Type)
                    );

                _ValueChanging = false;

                //refresh
                //Refresh();

                returnValue = true;
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);

                _ValueChanging = false;
                throw;
            }
            return returnValue;
        }

        /// <summary>
        /// Add an unassigned category to one of the sheet categories.
        /// </summary>
        /// <param name="sheet">Sheet</param>
        /// <param name="categoryName">string</param>
        /// <param name="categoryType">Category.SheetCategoryType</param>
        /// <returns>bool</returns>
        public static bool AddSheetCategory(Sheet sheet, string categoryName, Category.SheetCategoryType categoryType)
        {
            bool returnValue = default;
			try
			{
				//User must select a Category Item.
				CategoryItem categoryItem = SelectCategoryItem(sheet, categoryName, "Select a Category Item to Add to existing Sheet Cells.", 0, false);

				if (categoryItem != null)
                {
                    _ValueChanging = true;

                    //move it
                    sheet.AssignCategory(categoryName, categoryType, categoryItem);

                    _ValueChanging = false;

                    //refresh
                    //Refresh();

                    returnValue = true;
                }
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);

                _ValueChanging = false;
                throw;
            }
            return returnValue;
        }

        /// <summary>
        /// Changed an assigned category to another sheet category.
        /// </summary>
        /// <param name="sheet">Sheet</param>
        /// <param name="categoryName">string</param>
        /// <param name="categoryType">Category.SheetCategoryType</param>
        /// <returns>bool</returns>
        public static bool MoveSheetCategory(Sheet sheet, string categoryName, Category.SheetCategoryType categoryType)
        {
			bool returnValue;
			try
            {
                _ValueChanging = true;

                //move it
                sheet.AssignCategory(categoryName, categoryType, null);

                _ValueChanging = false;

                //refresh
                //Refresh();

                returnValue = true;
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);

                _ValueChanging = false;
                throw;
            }
            return returnValue;
        }

        /// <summary>
        /// Remove an assigned category from one of the sheet categories.
        /// </summary>
        /// <param name="sheet">Sheet</param>
        /// <param name="categoryName">string</param>
        /// <returns>bool</returns>
        public static bool RemoveSheetCategory(Sheet sheet, string categoryName)
        {
            bool returnValue = default;
			try
			{
				//User must select a Category Item.
				CategoryItem categoryItem = SelectCategoryItem(sheet, categoryName, "Select a Category Item to Remove from existing Sheet Cells. Note: Cells without selected Item will be permanently deleted.", 0, false);

				if (categoryItem != null)
                {
                    _ValueChanging = true;

                    //move it
                    sheet.AssignCategory(categoryName, Category.SheetCategoryType.None, categoryItem);

                    _ValueChanging = false;

                    //refresh
                    //Refresh();

                    returnValue = true;
                }
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);

                _ValueChanging = false;
                throw;
            }
            return returnValue;
        }

        /// <summary>
        /// Search until end or until another category item found; swap in latter case.
        /// </summary>
        /// <param name="sheet">Sheet</param>
        /// <param name="categoryName">string</param>
        /// <param name="categoryItemName">string</param>
        /// <param name="shiftType">ListOfTExtension.ShiftTypes</param>
        /// <returns>bool</returns>
        public static bool ShiftSheetCategoryItem(Sheet sheet, string categoryName, string categoryItemName, ListOfTExtension.ShiftTypes shiftType)
        {
            bool returnValue = default;
            Category category = default;

            try
            {
                _ValueChanging = true;

                //find item's parent
                category = sheet.Categories.Find(c => c.Name == categoryName);
                if (category == null)
                {
                    throw new ArgumentException(string.Format("Unable to find Category '{0}'.", categoryName));
                }

                category.Items.ShiftListItem<OrderedEquatableBindingList<CategoryItem>, CategoryItem>
                    (
                        shiftType,
                        (item) => item.Name == categoryItemName, //match on Category Item Name property
                        (__, _) => true //match on any Category Item)
                    );

                _ValueChanging = false;

                //refresh
                //Refresh();

                returnValue = true;
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);

                _ValueChanging = false;
                throw;
            }
            return returnValue;
        }

        /// <summary>
        /// Search until end or until another formula found; swap in latter case.
        /// </summary>
        /// <param name="sheet">Sheet</param>
        /// <param name="formulaName">string</param>
        /// <param name="shiftType">ListOfTExtension.ShiftTypes</param>
        /// <returns>bool</returns>
        public static bool ShiftSheetFormula(Sheet sheet, string formulaName, ListOfTExtension.ShiftTypes shiftType)
        {
            bool returnValue = default;

            try
            {
                _ValueChanging = true;

                sheet.Formulae.ShiftListItem<OrderedEquatableBindingList<Formula>, Formula>
                    (
                        shiftType,
                        item => item.Value == formulaName, //match on Formula Name property
                        (_, __) => true //match on any Formula)
                    );

                _ValueChanging = false;

                //refresh
                //Refresh();

                returnValue = true;
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);

                _ValueChanging = false;
                throw;
            }
            return returnValue;
        }

        /// <summary>
        /// Display dialog to allow user to select a category item from specified category.
        /// </summary>
        /// <param name="sheet">Sheet</param>
        /// <param name="categoryName">string</param>
        /// <param name="instructions">string</param>
        /// <param name="defaultSelectedIndex">int</param>
        /// <param name="useCategorySelectedItemIndex">bool</param>
        /// <returns>CategoryItem</returns>
        public static CategoryItem SelectCategoryItem(Sheet sheet, string categoryName, string instructions, int defaultSelectedIndex, bool useCategorySelectedItemIndex)
        {
            CategoryItem returnValue = default;
            Category category = default;
            string categoryItemName = default;

            try
            {
                //User must select a Category Item.
                //TODO:look into receiving a delegate that injects the UI method, with parameters for values below, and returning string or CategoryItem

                // // Display the form as a modal dialog box.
                // SelectDialog dialog = new SelectDialog();

                // //set dialog icon
                // dialog.Icon = MultiDimensionalSpreadsheetLibrary.Properties.Resources.MultiDimensionalSpreadsheet;

                //set dialog title
                // dialog.Text = "Select a Category Item";
                //TODO:output prompt

                //set instructions
                // dialog.lblInstructions.Text = instructions;
                //TODO:

                //set category name
                category = sheet.Categories.Find(c => c.Name == categoryName);
                // dialog.lblCategoryName.Text = category.Name;
                //TODO:output category name

                // Add  category items to the listbox
                // dialog.ddlCategoryItems.DataSource = category.Items;
                // dialog.ddlCategoryItems.DisplayMember = "Name";
                // dialog.ddlCategoryItems.ValueMember = "Name";
                //TODO:output list

                //pre-set selection
                if (useCategorySelectedItemIndex)
                {
                    // dialog.ddlCategoryItems.SelectedIndex = category.SelectedItemIndex;
                    //TODO:set selection to item passed
                }
                else if (defaultSelectedIndex != -1)
                {
                    // dialog.ddlCategoryItems.SelectedIndex = defaultSelectedIndex;
                    //TODO:set selection to default passed
                }

                //show dialog;
                // dialog.ShowDialog();
                //TODO:prompt user

                // Determine if the OK button was clicked on the dialog box.
                //TODO:check for response
                if (true)
                // if (dialog.DialogResult == DialogResult.OK)
                {
                    //TODO:look up item from user response
                    categoryItemName = "";//((CategoryItem)dialog.ddlCategoryItems.SelectedItem).Name;
                    // dialog.Dispose();
                    returnValue = category.Items.Find(ci => ci.Name == categoryItemName);
                }
                else
                {
                    // dialog.Dispose();
                }
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);

                throw;
            }
            return returnValue;
        }

        //public new static void Refresh()
        //{
        //    MDSSController<MDSSModel>.Model.IsChanged = true;//Value doesn't matter; fire a changed event;
        //}
        #endregion Methods
    }
}