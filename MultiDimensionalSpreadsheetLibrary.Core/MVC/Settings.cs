using System;
using System.ComponentModel;
using System.Reflection;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using Ssepan.Application.Core;
using Ssepan.Collections.Core;
using Ssepan.Utility.Core;

namespace MultiDimensionalSpreadsheetLibrary.Core.MVC
{
	/// <summary>
	/// Summary description for Settings.
	/// </summary>
	//[Serializable()]
	[TypeConverter(typeof(ExpandableObjectConverter))]
    [DataContract(IsReference = true)]
    public class Settings :
        SettingsBase
    {
        #region declarations
        public const string FILE_TYPE_EXTENSION = "mdss";
        public const string FILE_TYPE_NAME = "mdssfile";
        public const string FILE_TYPE_DESCRIPTION = "MultiDimensionalSpreadsheet Settings File";
        #endregion declarations

        #region constructors
        public Settings()
        {
            try
            {
                FileTypeExtension = FILE_TYPE_EXTENSION;
                FileTypeName = FILE_TYPE_NAME;
                FileTypeDescription = FILE_TYPE_DESCRIPTION;
                SerializeAs = SerializationFormat.DataContract;//non-default
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
                throw;
            }
        }
        #endregion

        #region IDisposable support
        ~Settings()
        {
            Dispose(false);
        }

        //inherited; override if additional cleanup needed
        protected override void Dispose(bool disposeManagedResources)
        {
            // process only if managed and unmanaged resources have
            // not been disposed of.
            if (!disposed)
            {
                try
                {
                    //Resources not disposed
                    if (disposeManagedResources)
                    {
                        // dispose managed resources
                    }

                    disposed = true;
                }
                finally
                {
                    // dispose unmanaged resources
                    base.Dispose(disposeManagedResources);
                }
            }
            else
            {
                //Resources already disposed
            }
        }
        #endregion

        #region IEquatable<ISettings> Members
        /// <summary>
        /// Compare property values of two specified Settings objects.
        /// </summary>
        /// <param name="other">ISettingsComponent</param>
        /// <returns>bool</returns>
        public override bool Equals(ISettingsComponent other)
        {
			bool returnValue;
			try
			{
				Settings otherSettings = other as Settings;
				if (this == otherSettings)
				{
					returnValue = true;
				}
				else if (!base.Equals(other))
				{
					returnValue = false;
				}
				else if (Version != otherSettings.Version)
				{
					returnValue = false;
				}
				else if (!Sheets.Equals(otherSettings.Sheets))
				{
					returnValue = false;
				}
				else
				{
					returnValue = true;
				}
			}
			catch (Exception ex)
			{
				Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
				throw;
			}

			return returnValue;
        }
        #endregion IEquatable<ISettings> Members

        #region ListChanged handlers
        void Sheets_ListChanged(object sender, ListChangedEventArgs e)
        {
            try
            {
                OnPropertyChanged(string.Format("Settings.Sheets[{0}].{1}", e.NewIndex, e.PropertyDescriptor == null ? string.Empty : e.PropertyDescriptor.Name));
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);

                throw;
            }
        }
        #endregion ListChanged handlers

        #region Properties
        [XmlIgnore]
        public override bool Dirty
        {
            get
            {
				bool returnValue;
				try
                {
                    if (base.Dirty)
                    {
                        returnValue = true;
                    }
                    else if (_Sheets.Count != __Sheets.Count) //(!_Sheets.Equals(__Sheets)) //cloning via DataContract serialize-deserialize resulting in unique items?
                    {
                        returnValue = true;
                    }
                    else if (_Version != __Version)
                    {
                        returnValue = true;
                    }
                    else
                    {
                        returnValue = false;
                    }
                }
                catch (Exception ex)
                {
                    Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);

                    throw;
                }

                return returnValue;
            }
        }

        #region Persisted Properties

        private EquatableBindingList<Sheet> __Sheets = [];
        private EquatableBindingList<Sheet> _Sheets = [];
        [DataMember]
        public EquatableBindingList<Sheet> Sheets
        {
            get { return _Sheets; }
            set
            {
                if (_Sheets != null)
                {
                    _Sheets.ListChanged -= Sheets_ListChanged;
                }
                _Sheets = value;
                if (_Sheets != null)
                {
                    _Sheets.ListChanged += Sheets_ListChanged;
                }
                OnPropertyChanged(nameof(Sheets));
            }
        }

        private string __Version = "0";
        private string _Version = "0";
        [Description("Application major version"),
		Category("Misc"),
		DefaultValue(null)]
        [DataMember]
        public string Version
        {
            get { return _Version; }
            set
            {
                _Version = value;
                OnPropertyChanged(nameof(Version));
            }
        }
        #endregion Persisted Properties
        #endregion Properties

        #region Methods

        /// <summary>
        /// Copies property values from source working fields to destination working fields, then optionally syncs destination.
        /// <param name="destinationSettings"></param>
        /// <param name="sync"></param>
        /// </summary>
        public override void CopyTo(ISettingsComponent destination, bool sync)
        {
			try
			{
				Settings destinationSettings = destination as Settings;

				destinationSettings.Sheets = Sheets;
                destinationSettings.Version = Version;

                base.CopyTo(destination, sync);//also checks and optionally performs sync
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);

                throw;
            }
        }

        /// <summary>
        /// Syncs property values from working fields to reference fields.
        /// </summary>
        public override void Sync()
        {
            try
            {
                __Version = _Version;
                __Sheets = ObjectHelper.Clone(_Sheets, Ssepan.Utility.Core.SerializationFormat.DataContract);

                //DEBUG:(breaks Dirty flag)
                //force Dirty to False in sheets Equal comparison 
                base.Sync();

                //Note:where we have cloned collections; the collection comparison will never find the original items in the cloned collection if it is looking at identity vs content--SJS
                //if (Dirty)
                //{
                //    throw new ApplicationException("Sync failed.");
                //}
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);

                throw;
            }
        }
        #endregion Methods
    }
}
