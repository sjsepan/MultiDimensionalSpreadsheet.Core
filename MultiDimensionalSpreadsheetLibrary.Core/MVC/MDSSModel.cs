﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using Ssepan.Application.Core;
using Ssepan.Collections.Core;
using System.Reflection;
using Ssepan.Utility.Core;

namespace MultiDimensionalSpreadsheetLibrary.Core.MVC
{
	/// <summary>
	/// run-time model; relies on settings
	/// </summary>
	//[DefaultPropertyAttribute("SomePropertyName")]
	[TypeConverter(typeof(ExpandableObjectConverter))]
    public class MDSSModel :
        ModelBase
    {
        #region Constructors
        public MDSSModel()
        {
            if (SettingsController<Settings>.Settings == null)
            {
                SettingsController<Settings>.New();
            }
            Debug.Assert(SettingsController<Settings>.Settings != null, "SettingsController<Settings>.Settings != null");
        }
        #endregion Constructors

        #region IEquatable<IModel>
        /// <summary>
        /// Compare property values of two specified Model objects.
        /// </summary>
        /// <param name="other">IModelComponent</param>
        /// <returns>Boolean</returns>
        public override bool Equals(IModelComponent other)
        {
			bool returnValue;
			try
			{
				MDSSModel otherModel = other as MDSSModel;
				if (this == otherModel)
				{
					returnValue = true;
				}
				else if (!base.Equals(other))
				{
					returnValue = false;
				}
				else if (!Sheets.Equals(otherModel.Sheets))
				{
					returnValue = false;
				}
				else if (Version != otherModel.Version)
				{
					returnValue = false;
				}
				else
				{
					returnValue = true;
				}
			}
			catch (Exception ex)
			{
				Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
				throw;
			}

			return returnValue;
        }
        #endregion IEquatable<IModel>

        #region ListChanged handlers
        //void Sheets_ListChanged(object sender, ListChangedEventArgs e)
        //{
        //    try
        //    {
        //        this.OnPropertyChanged(string.Format("Sheet.Cells[{0}].{1}", e.NewIndex, (e.PropertyDescriptor == null ? string.Empty : e.PropertyDescriptor.Name)));
        //    }
        //    catch (Exception ex)
        //    {
        //        Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);

        //        throw;
        //    }
        //}
        #endregion ListChanged handlers

        #region AddingNew handlers
        //void Sheets_AddingNew(object sender, AddingNewEventArgs e)
        //{
        //    try
        //    {
        //        e.NewObject = new Sheet(Sheet.NewItemName);
        //    }
        //    catch (Exception ex)
        //    {
        //        Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);

        //        throw;
        //    }
        //}
        #endregion AddingNew handlers

        #region Properties
        private string[] _Args;
        public string[] Args
        {
            get { return _Args; }
            set
            {
                _Args = value;
                OnPropertyChanged(nameof(Args));
            }
        }

        public EquatableBindingList<Sheet> Sheets
        {
            get { return SettingsController<Settings>.Settings.Sheets; }
            set
            {
                SettingsController<Settings>.Settings.Sheets = value;
                OnPropertyChanged(nameof(Sheets));
            }
        }

        public string Version
        {
            get { return SettingsController<Settings>.Settings.Version; }
            set
            {
                SettingsController<Settings>.Settings.Version = value;
                OnPropertyChanged(nameof(Version));
            }
        }
        #endregion Properties

    }
}
