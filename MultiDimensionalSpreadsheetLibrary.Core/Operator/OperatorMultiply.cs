﻿namespace MultiDimensionalSpreadsheetLibrary.Core
{
	public class OperatorMultiply :
        OperatorBase
    {
        public const char Operator = '*';

        public OperatorMultiply()
        {
            Name = Operator.ToString();
            Precedence = 0;

            //define Multiply for OperandBase
            functorOperandBase = (dictionary, context) => new OperandLiteral(dictionary["Left"].Evaluate(context) * dictionary["Right"].Evaluate(context));
        }
    }
}
