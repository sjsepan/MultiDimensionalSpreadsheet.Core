﻿namespace MultiDimensionalSpreadsheetLibrary.Core
{
	public class OperatorAdd :
        OperatorBase
    {
        public const char Operator = '+';

        public OperatorAdd()
        {
            Name = Operator.ToString();
            Precedence = 1;

            //define Add for OperandBase
            functorOperandBase = (dictionary, context) => new OperandLiteral(dictionary["Left"].Evaluate(context) + dictionary["Right"].Evaluate(context));
        }
    }
}
