﻿namespace MultiDimensionalSpreadsheetLibrary.Core
{
	public class OperatorAssignment :
        OperatorBase
    {
        public const char Operator = '=';

        public OperatorAssignment()
        {
            Name = Operator.ToString();
            Precedence = -1;

            //define Assignment for OperandBase
            functorOperandBase = null;
        }
    }
}
