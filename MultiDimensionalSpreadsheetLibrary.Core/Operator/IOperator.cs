﻿using System.Collections.Generic;

namespace MultiDimensionalSpreadsheetLibrary.Core
{
	/// <summary>
	/// Defines function to be performed.
	/// </summary>
	public interface IOperator
    {
        OperandBase Run(Dictionary<string, OperandBase> dictionary, FormulaExecutionContext context);
    }
}
