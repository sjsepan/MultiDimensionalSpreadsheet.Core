﻿namespace MultiDimensionalSpreadsheetLibrary.Core
{
	public class OperatorSubtract :
        OperatorBase
    {
        public const char Operator = '-';

        public OperatorSubtract()
        {
            Name = Operator.ToString();
            Precedence = 1;

            //define Subtract for OperandBase
            functorOperandBase = (dictionary, context) => new OperandLiteral(dictionary["Left"].Evaluate(context) - dictionary["Right"].Evaluate(context));
        }
    }
}
