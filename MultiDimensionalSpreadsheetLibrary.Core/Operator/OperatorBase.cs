﻿using System;
using System.Collections.Generic;
using Ssepan.Utility.Core;
using System.Reflection;

namespace MultiDimensionalSpreadsheetLibrary.Core
{
	public abstract class OperatorBase :
        IOperator
    {
		#region Properties
		/// <summary>
		/// The operator name (and character(s)).
		/// </summary>
		public string Name { get; set; }
		/// <summary>
		/// The operator's precedence relative to all other operators.
		/// </summary>
		public int Precedence { get; set; }

		/// <summary>
		/// The operation's OperandBase implementation is stored here.
		/// </summary>
		public Func<Dictionary<string, OperandBase>, FormulaExecutionContext, OperandBase> functorOperandBase;
        #endregion Properties

        #region IOperator Members
        /// <summary>
        /// The operation's OperandBase implementation is invoked here.
        /// </summary>
        /// <param name="dictionary">Dictionary<string, OperandBase></param>
        /// <param name="context">FormulaExecutionContext</param>
        /// <returns>OperandBase</returns>
        public OperandBase Run(Dictionary<string, OperandBase> dictionary, FormulaExecutionContext context)
        {
            OperandBase returnValue = default;

            try
            {
                returnValue = functorOperandBase(dictionary, context);
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);

                throw;
            }
            return returnValue;
        }
        #endregion IOperator Members
    }
}
