﻿using System;

namespace MultiDimensionalSpreadsheetLibrary.Core
{
	public class OperatorMax :
        OperatorBase
    {
        public const string Operator = "MAX";

        public OperatorMax()
        {
            Name = Operator;
            Precedence = -1;

            //define Max for OperandBase
            functorOperandBase = (dictionary, context) => new OperandLiteral(Math.Max(dictionary["Value0"].Evaluate(context), dictionary["Value1"].Evaluate(context)));
        }
    }
}
