﻿using System;

namespace MultiDimensionalSpreadsheetLibrary.Core
{
	public class OperatorAbs :
        OperatorBase
    {
        public const string Operator = "ABS";

        public OperatorAbs()
        {
            Name = Operator;
            Precedence = -1;

            //define Abs for OperandBase
            functorOperandBase = (dictionary, context) => new OperandLiteral(Math.Abs(dictionary["Value0"].Evaluate(context)));
        }
    }
}
