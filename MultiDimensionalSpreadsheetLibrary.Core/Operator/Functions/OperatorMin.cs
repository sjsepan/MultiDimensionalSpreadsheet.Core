﻿using System;

namespace MultiDimensionalSpreadsheetLibrary.Core
{
	public class OperatorMin :
        OperatorBase
    {
        public const string Operator = "MIN";

        public OperatorMin()
        {
            Name = Operator;
            Precedence = -1;

            //define Min for OperandBase
            functorOperandBase = (dictionary, context) => new OperandLiteral(Math.Min(dictionary["Value0"].Evaluate(context), dictionary["Value1"].Evaluate(context)));
        }
    }
}
