﻿namespace MultiDimensionalSpreadsheetLibrary.Core
{
	public class OperatorDivide :
        OperatorBase
    {
        public const char Operator = '/';

        public OperatorDivide()
        {
            Name = Operator.ToString();
            Precedence = 0;

            //define Divide for OperandBase
            functorOperandBase = (dictionary, context) => new OperandLiteral(dictionary["Left"].Evaluate(context) / dictionary["Right"].Evaluate(context));
        }
    }
}
