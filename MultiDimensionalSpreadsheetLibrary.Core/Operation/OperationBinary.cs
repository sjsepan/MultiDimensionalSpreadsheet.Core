﻿using System;
using System.Collections.Generic;
using Ssepan.Utility.Core;
using System.Reflection;

namespace MultiDimensionalSpreadsheetLibrary.Core
{
	public class OperationBinary :
        OperationBase
    {
        #region Constructors
        public OperationBinary()
        {
                Operands = new Dictionary<string, OperandBase>
				{
					{ "Left", null },
					{ "Right", null }
				};
        }
        #endregion Constructors

        #region IOperation Members
        /// <summary>
        /// Run operation on child operands. Must pass through sheet cell and formula to child operands that may need to do cell lookups.
        /// </summary>
        /// <param name="context">FormulaExecutionContext</param>
        /// <returns>float</returns>
        public override float Run(FormulaExecutionContext context)
        {
			float returnValue;
			try
			{
				//The operand (of type OperandOperation) that contains this operation will have its Evaluate() called; Evaluate() will call this operation's Run().
				OperandBase operationResult = Operator.Run(Operands, context);
				returnValue = operationResult.Evaluate(context);
			}
			catch (Exception ex)
			{
				Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);

				throw;
			}
			return returnValue;
        }
        #endregion IOperation Members
    }
}
