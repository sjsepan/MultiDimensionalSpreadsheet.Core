﻿using System.Collections.Generic;

namespace MultiDimensionalSpreadsheetLibrary.Core
{
	public abstract class OperationBase :
        IOperation
    {
		#region Properties
		public OperatorBase Operator { get; set; }
		public Dictionary<string, OperandBase> Operands { get; set; }
		#endregion Properties

		#region IOperation Members
		/// <summary>
		/// Run operation on child operands. Must pass through sheet cell and formula to child operands that may need to do cell lookups.
		/// </summary>
		/// <param name="context">FormulaExecutionContext</param>
		/// <returns>float</returns>
		public abstract float Run(FormulaExecutionContext context);
        #endregion IOperation Members
    }
}
