﻿namespace MultiDimensionalSpreadsheetLibrary.Core
{
	/// <summary>
	/// Defines context of operation (such as parameters).
	/// </summary>
	public interface IOperation
    {
		float Run(FormulaExecutionContext context);
    }
}
