﻿using System;
using Ssepan.Utility.Core;
using System.Reflection;

namespace MultiDimensionalSpreadsheetLibrary.Core
{
	/// <summary>
	/// Information about a given formula execution; needed by child objects (operations, operators, operands)
	/// </summary>
	public class FormulaExecutionContext
    {
        public FormulaExecutionContext()
        {
        }
        public FormulaExecutionContext(Formula formula, SheetCell cell)
        {
            try
            {
                ExecutingFormula = formula;
                AssigneeCell = cell;
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
                throw;
            }
        }
		public Formula ExecutingFormula { get; set; }
		public SheetCell AssigneeCell { get; set; }
	}
}
