﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.Serialization;
using System.Linq;
using System.Text.RegularExpressions;
using System.Xml.Serialization;
using System.Reflection;
using Ssepan.Application.Core;

using Ssepan.Collections.Core;
using Ssepan.Utility.Core;
using MultiDimensionalSpreadsheetLibrary.Core.MVC;

namespace MultiDimensionalSpreadsheetLibrary.Core
{
	/// <summary>
    /// Formula
	/// </summary>
	//[Serializable()]
	[DataContract(IsReference = true)]
    [TypeConverter(typeof(ExpandableObjectConverter))]
    public class Formula :
        SettingsComponentBase,
        IEquatable<Formula>,
        INotifyPropertyChanged
    {
        #region Declarations

        private static readonly List<OperatorBase> arithmeticOperators;
        private static readonly List<OperatorBase> functionOperators;

        //private const string RegExFindOutermostParenthesisContents = @"(?<!\\)\((\\\(|\\\)|[^\(\)]|(?<!\\)\(.*(?<!\\)\))*(?<!\\)\)";
        private const string RegExFindInnermostParenthesisAndContents = @"\([^\(\)]*\)";
        private const string RegExFindInnermostBracesAndContents = @"\{[^\{\}]*\}";
        private const string RegExFindFunctionNameDelimitersAndContents = @"\@[^\@\{]*\{";
        private const char LeftParenthesis = '(';
        private const char RightParenthesis = ')';
        private const char LeftBrace = '{';
        private const char RightBrace = '}';
        private const string ParsingVariableIndicator = "#";
        private const char FunctionParameterDelimiter = ',';

        public const string NewItemName = "(new formula)";
        public const string FunctionIndicator = "@"; //Reserved
        public const string VariableIndicator = "$"; //Reserved
        /*
        y=(p+q)*((r-s)/t)
        */
        #endregion Declarations

        #region Constructors
        static Formula()
        {
            try
            {
                //create a list of arithmetic operators found on the right side of an assignment; sort them by precedence, descending
                arithmeticOperators =
				[
					new OperatorAdd(),
					new OperatorSubtract(),
					new OperatorMultiply(),
					new OperatorDivide(),
				];
                arithmeticOperators.Sort((operator1, operator2) => operator1.Precedence.CompareTo(operator2.Precedence));
                arithmeticOperators.Reverse();

                //create a list of function operators found on the right side of an assignment
                functionOperators = [new OperatorAbs(), new OperatorMax(), new OperatorMin()];
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
                throw;
            }
        }

        public Formula()
        {
            try
            {
                //ParsingVariables = new Dictionary<String, String>();
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
                throw;
            }
        }

        /// <summary>
        /// Formula
        /// </summary>
        /// <param name="value">string</param>
        public Formula(string value) :
            this()
        {
            try
            {
                Value = value;
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
                throw;
            }
        }

        /// <summary>
        /// Parent  is the sheet
        /// </summary>
        /// <param name="value">string</param>
        /// <param name="parent">Sheet</param>
        public Formula(string value, Sheet parent) :
            this(value)
        {
            try
            {
                Parent = parent;
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
                throw;
            }
        }
        #endregion Constructors

        #region INotifyPropertyChanged support
        new public event PropertyChangedEventHandler PropertyChanged;

        new void OnPropertyChanged(string propertyName)
        {
            try
            {
				PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
			}
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
                //throw;
            }
        }
        #endregion INotifyPropertyChanged support

        #region IEquatable<Formula>
        /// <summary>
        /// Compare property values of this object to another.
        /// </summary>
        /// <param name="other">Formula</param>
        /// <returns>bool</returns>
        public bool Equals(Formula other)
        {
			bool returnValue;
			try
            {
                if (this == other)
                {
                    returnValue = true;
                }
                else
                {
					returnValue = Value == other.Value;
				}
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
                throw;
            }

            return returnValue;
        }

		#endregion IEquatable<Formula>
		#region Properties
		#region NonPersisted Properties
		[XmlIgnore]
		public string ErrorMessage { get; set; }

		[XmlIgnore]
        public bool IsValid
        {
            get
            {
                bool returnValue = default;
                try
                {
                    if (Valid())
                    {
                        returnValue = true;
                    }
                }
                catch (Exception ex)
                {
                    Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
                }
                return returnValue;
            }
            //set { _Valid = value; }
        }

        [XmlIgnore]
        public bool IsCompiled
        {
            get
            {
                bool returnValue = default;
                try
                {
                    if (Compiled())
                    {
                        returnValue = true;
                    }
                }
                catch (Exception ex)
                {
                    Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
                }
                return returnValue;
            }
        }
		/// <summary>
		/// Category item criteria identifying cells to which the formula will apply.
		/// It is built when formula is validated.
		/// When a cell needs a value, AssigneeCriteria will be run
		///  and the specific cell sought in that list.
		/// If found, the AssignerOperand is evaluated and the result assigned to the cell.
		/// </summary>
		[XmlIgnore]
		public List<CategoryItem> AssigneeCriteria { get; set; }
		/// <summary>
		/// The operand that will be evaluated for a given matching cell.
		/// It is composed when the formula is validated.
		/// When a cell needs a value, and it matches the assignee criteria for this formula,
		///  the root assigner operand for this formula is evaluated.
		/// 1) Literal operands return  the value that they contain.
		/// 2) Reference operands use their own category item criteria
		///  to narrow down the list of cells to which the formula applies. Then the specific cell's criteria
		///  is used to identify categories not in the criteria, and use those to narrow down to a single source cell.
		/// 3) Operation operands perform their operation and trigger the evaluation of their child operands.
		/// </summary>
		[XmlIgnore]
		public OperandBase AssignerOperand { get; set; }
		/// <summary>
		/// Variables used for substitution during parsing.
		/// For now, these are only used for pre-parsing expressions in parenthesis.
		/// </summary>
		[XmlIgnore]
		public Dictionary<String, String> ParsingVariables { get; set; } = [];

		//TODO:Need temp value for a cell that needs a value; must be reentrant! 
		// Keep a list, add when starting and remove when finished; can also use to check for circular references.
		#endregion NonPersisted Properties

		#region Persisted Properties
		private string _Value;
        [DataMember]
        public string Value
        {
            get { return _Value; }
            set
            {
                _Value = value;
                OnPropertyChanged(nameof(Value));

                //any change to the formula source invalidates the compiled formula
                Delegate = default;
            }
        }

        private Sheet _Parent;
        [DataMember]
        public Sheet Parent
        {
            get { return _Parent; }
            set
            {
                _Parent = value;
                OnPropertyChanged(nameof(Parent));
            }
        }

        private Delegate _Delegate;
        [DataMember]
        public Delegate Delegate
        {
            get { return _Delegate;  }
            set
            {
                _Delegate = value;
                OnPropertyChanged(nameof(Delegate));
            }
        }
        #endregion Persisted Properties
        #endregion Properties

        #region Methods
        #region Public Methods
        /// <summary>
        /// validate settings entered by user
        /// because this is being called by grid databinding, it is run on every refresh
        /// </summary>
        public bool Valid()
        {
            bool returnValue = default;
            string errorMessage = default;

            try
            {
                ErrorMessage = string.Empty;

                if (string.IsNullOrEmpty(Value))
                {
                    ErrorMessage = string.Format("Formula Value must contain a value.");
                }
                //don't force re-parsing of the source if the delegate is compiled
                else if (!Compiled())
                {
                    if (!Parse(Value, ref errorMessage))
                    {
                        ErrorMessage = string.Format("The formula is not valid. \n'{0}': '{1}'", errorMessage, Value);
                    }
                    else
                    {
                        returnValue = true;
                    }
                }
                else
                {
                    returnValue = true;
                }
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
                throw;
            }

            return returnValue;
        }

        /// <summary>
        /// check for presence of compiled expression
        /// </summary>
        public bool Compiled()
        {
            bool returnValue = default;

            try
            {
                ErrorMessage = string.Empty;

                if (Delegate == default)
                {
                    ErrorMessage = string.Format("The formula is not compiled.\n{0}", Value);
                }
                else
                {
                    returnValue = true;
                }
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
                throw;
            }

            return returnValue;
        }

        /// <summary>
        /// check for presence of assignee operand
        /// </summary>
        public bool HasAssignee()
        {
			bool returnValue;
			try
            {
                ErrorMessage = string.Empty;

                if ((AssigneeCriteria == null) || (AssigneeCriteria.Count == 0))
                {
                    returnValue = false;
                    ErrorMessage = string.Format("Assignee criteria not defined.");
                }
                else
                {
                    returnValue = true;
                }
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
                throw;
            }

            return returnValue;
        }

        /// <summary>
        /// check for presence of assigner operand
        /// </summary>
        public bool HasAssigner()
        {
			bool returnValue;
			try
            {
                ErrorMessage = string.Empty;

                if (AssignerOperand == null)
                {
                    returnValue = false;
                    ErrorMessage = string.Format("Assigner operand not defined.");
                }
                else
                {
                    returnValue = true;
                }
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
                throw;
            }

            return returnValue;
        }

        /// <summary>
        /// validate settings determined at the time of execution
        /// </summary>
        public bool Complete()
        {
			bool returnValue;
			try
            {
                ErrorMessage = string.Empty;

                if (!Compiled())
                {
                    returnValue = false;
                }
                else if (!Valid())
                {
                    returnValue = false;
                }
                else if (!HasAssignee())
                {
                    returnValue = false;
                }
                else if (!HasAssigner())
                {
                    returnValue = false;
                }
                else
                {
                    returnValue = true;
                }
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
                throw;
            }

            return returnValue;
        }

        /// <summary>
        /// Determine if passed cell is among cells to which the formula criteria are applicable.
        /// </summary>
        /// <param name="cell">Specific sheet cell seeking value.</param>
        /// <returns>bool</returns>
        public bool IsApplicableFormula(SheetCell cell)
        {
            bool returnValue = default;
			try
			{
                if (Complete())
                {
					//locate parent sheet, containing cells
					Sheet sheet = Parent ?? throw new ApplicationException(string.Format("Unable to find parent sheet for formula: '{0}'", Value));

					//run formula assigner criteria
					List<SheetCell> formulaSearchResults = Sheet.GetCellsWhereAllSearchCategoryItemsMatchAnyInGivenCell([.. sheet.Cells], AssigneeCriteria);
					if ((formulaSearchResults == null) || (formulaSearchResults.Count == 0))
                    {
                        throw new ApplicationException(string.Format("Unable to find cells for parent sheet of formula '{0}'", Value));
                    }

					//find single cell in results
					List<SheetCell> cellSearchResults = Sheet.GetCellsWhereAllSearchCategoryItemsMatchAnyInGivenCell(formulaSearchResults, [.. cell.CategoryItems]);
					if ((cellSearchResults == null) || (cellSearchResults.Count == 0))
                    {
                        //cell not found; not an error
                    }
                    else
                    {
                        returnValue = true;
                    }
                }
                else
                {
                    //skip over any invalid formulae
                    Log.Write(ErrorMessage, Log.EventLogEntryType_Error);
                }
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
                //throw;
            }
            return returnValue;
        }

        /// <summary>
        /// Run formula and apply to passed cell.
        /// </summary>
        /// <param name="cell">SheetCell</param>
        /// <returns>bool</returns>
        public bool Run(SheetCell cell)
        {
			bool returnValue;
			try
			{
				//validate settings determined at runtime
				if (!Complete())
				{
					throw new ApplicationException(string.Format("{0}", ErrorMessage));
				}

				//run operation
				FormulaExecutionContext context = new(this, cell);
				cell.ValueChangingProgrammatically = true;
				//TODO:invoke with parameter list
				cell.Value = Delegate.DynamicInvoke([]).ToString(); //AssignerOperand.Evaluate(context).ToString();
                                                                        //TODO:move this or something like it to where formula is changed.
                                                                        //cell.Value = AssignerOperand.Evaluate(context).ToString();
				cell.ValueChangingProgrammatically = false;

				returnValue = true;
			}
			catch (Exception ex)
			{
				Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
				cell.ValueChangingProgrammatically = false;
				throw;
			}
			return returnValue;
        }
        #endregion Public Methods

        #region Private Methods
        /// <summary>
        /// Parse the formula to get assignee and assigner.
        /// </summary>
        /// <param name="value">string</param>
        /// <param name="errorMessage">ref string</param>
        /// <returns>bool</returns>
        private bool Parse(string value, ref string errorMessage)
        {
            bool returnValue = default;
			try
			{
				string[] tokens = value.Split(OperatorAssignment.Operator);
				if (tokens.Length != 2)
                {
                    throw new ArgumentException(string.Format("Incorrectly formatted formula; there does not appear to be an assignment: '{0}'", value));
                }

				string assigneeToken = tokens[0].Trim();
				if (!ParseAssignee(assigneeToken, ref errorMessage))
                {
                    throw new ApplicationException(string.Format("Unable to parse assignee: '{0}'", assigneeToken));
                }

				string assignerToken = tokens[1].Trim();
				if (!ParseAssigner(assignerToken, ref errorMessage))
                {
                    throw new ApplicationException(string.Format("Unable to parse assigner: '{0}'", assignerToken));
                }

                returnValue = true;
            }
            catch (Exception ex)
            {
                errorMessage = ex.Message;
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
                //throw;
            }
            return returnValue;
        }

        /// <summary>
        /// Parse the assignee to get the category item criteria list
        /// </summary>
        /// <param name="assigneeToken">string</param>
        /// <param name="errorMessage">ref string</param>
        /// <returns>bool</returns>
        private bool ParseAssignee(string assigneeToken, ref string errorMessage)
        {
            bool returnValue = default;
            Sheet parentSheet = default;
            List<CategoryItem> criteria = default;

            try
            {
                if (assigneeToken?.Length == 0)
                {
                    throw new ArgumentException(string.Format("Incorrectly formatted assignee; the assignee address is not defined: '{0}'", assigneeToken));
                }
                assigneeToken = assigneeToken.Trim();

                parentSheet = SettingsController<Settings>.Settings.Sheets.Find(s => s.Equals(Parent));
                //parentSheet = SettingsController<Settings>.Settings.Sheets.Find(s => s.Name == this.Parent);
                if (parentSheet == null)
                {
                    throw new ApplicationException(string.Format("Unable to find sheet '{0}' for formula '{1}'.", Parent, Value));
                }

                criteria = parentSheet.GetCategoryItemsForAddressName(assigneeToken, true);
                if ((criteria == null) || (criteria.Count < 1))
                {
                    throw new ApplicationException(string.Format("Unable to find category-item criteria in sheet '{0}' for formula '{1}'.", Parent, Value));
                }

                //store criteria
                AssigneeCriteria = criteria;

                returnValue = true;
            }
            catch (Exception ex)
            {
                errorMessage = ex.Message;
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
                //throw;
            }
            return returnValue;
        }

        /// <summary>
        /// Parse the assigner to get the tree of operations and their operands (of type literal, reference, or operation)
        /// </summary>
        /// <param name="assignerToken">string</param>
        /// <param name="errorMessage">ref string</param>
        /// <returns>bool</returns>
        private bool ParseAssigner(string assignerToken, ref string errorMessage)
        {
            bool returnValue = default;
            OperandBase operand = default;

            try
            {
                if (assignerToken?.Length == 0)
                {
                    throw new ArgumentException(string.Format("Incorrectly formatted formula; the assigner is not defined: '{0}'", assignerToken));
                }

                //init parsing variables dictionary
                if (ParsingVariables == null)
                {
                    ParsingVariables = [];
                }
                else
                {
                    ParsingVariables.Clear();
                }

                //pre-parse token for parenthesis, and replace expressions in parenthesis with variables
                if (!ParseParenthesis(ref assignerToken, ref errorMessage))
                {
                    throw new ApplicationException(errorMessage);
                }

                //recursively parse assignerToken and build operation tree
                if (!ParseToken(assignerToken, ref operand, ref errorMessage))
                {
                    throw new ApplicationException(errorMessage);
                }

                //clear parsing variables dictionary
                ParsingVariables.Clear();

                AssignerOperand = operand;

                returnValue = true;
            }
            catch (Exception ex)
            {
                errorMessage = ex.Message;
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
                //throw;
            }
            return returnValue;
        }

        /// <summary>
        /// Pre-parse the token for parenthesis, and replace (from the inside out)
        /// each expression in parenthesis with a variable that acts as a placeholder.
        /// The expression is stored in a dictionary identified by @variable_name.
        /// The variable is used later during regular parsing to look up the expression.
        /// </summary>
        /// <param name="assignerToken">ref string</param>
        /// <param name="errorMessage">ref string</param>
        /// <returns>bool</returns>
        private bool ParseParenthesis(ref string assignerToken, ref string errorMessage)
        {
            bool returnValue = default;
            Regex regEx = default;
            int countOfLeftParenthesis = default;
            int countOfRightParenthesis = default;
            Match match = default;
            string variableName = default;
            string variableValue = default;

            try
            {
                countOfLeftParenthesis = (from char ch in assignerToken
                                          where ch == LeftParenthesis
										  select ch).Count();
                countOfRightParenthesis = (from char ch in assignerToken
                                           where ch == RightParenthesis
										   select ch).Count();
                if ((countOfLeftParenthesis > 0) || (countOfRightParenthesis > 0))
                {
                    //parenthesis found

                    if (countOfLeftParenthesis != countOfRightParenthesis)
                    {
                        //mismatched pair
                        throw new ApplicationException(string.Format("Mis-matched parenthesis in token: '{0}'", assignerToken));
                    }

                    regEx = new Regex(RegExFindInnermostParenthesisAndContents);
                    while (assignerToken.Contains(LeftParenthesis) || assignerToken.Contains(RightParenthesis))
                    {
                        //process until there are no more pairs left

                        match = regEx.Match(assignerToken);
                        if (string.IsNullOrEmpty(match.Value))
                        {
                            //pair did not match pattern
                            throw new ApplicationException(string.Format("Parenthesis in token not formatted correctly: '{0}'", assignerToken));
                        }

                        //generate variable
                        variableName = string.Format("{0}{1}", ParsingVariableIndicator, Guid.NewGuid().ToString().Replace('-'.ToString(), string.Empty));
                        variableValue = match.Value.Replace(LeftParenthesis.ToString(), string.Empty).Replace(RightParenthesis.ToString(), string.Empty);
                        if (string.IsNullOrEmpty(variableValue))
                        {
                            //pair did not contain an expression
                            throw new ApplicationException(string.Format("Value in parenthesis in token not formatted correctly: '{0}'", assignerToken));
                        }

                        //store variable in dictionary
                        ParsingVariables.Add(variableName, variableValue);

                        //replace expression (including parenthesis) in token w/ variable name
                        assignerToken = assignerToken.Replace(match.Value, variableName);
                    }
                }

                returnValue = true;
            }
            catch (Exception ex)
            {
				errorMessage = ex.Message;
				Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
                //throw;
            }
            return returnValue;
        }

        /// <summary>
        /// Parse the string token to get an operand of type Literal, Reference, or Operation.
        /// 1) Literal operands return  the value that they contain.
        /// 2) Reference operands use their own category item criteria
        ///  to narrow down the list of cells to which the formula applies. Then the specific cell's criteria
        ///  is used to identify categories not in the criteria, and use those to narrow down to a single source cell.
        /// 3) Operation operands perform their operation and trigger the evaluation of their child operands.
        /// </summary>
        /// <param name="token">string</param>
        /// <param name="operand">ref OperandBase</param>
        /// <param name="errorMessage">ref string</param>
        /// <returns>bool</returns>
        private bool ParseToken(string token, ref OperandBase operand, ref string errorMessage)
        {
            bool returnValue = default;
			bool isFoundOperator = default;
			OperatorBase foundOperator = default;
            OperandBase leftOperand = default;
            OperandBase rightOperand = default;
			try
			{
                //look for operators, if any, in reverse precedence order
                foreach (OperatorBase op in arithmeticOperators)
                {
                    if (token.Contains(op.Name))
                    {
                        isFoundOperator = true;
                        foundOperator = op;

                        break;
                    }
                }

				string[] tokens;
				//create an array of one or two sub-tokens
				if (isFoundOperator)
				{
					//split into 2 parts only
					tokens = token.Split([foundOperator.Name[0]], 2);
				}
				else
				{
					//create list with single item
					tokens = ([token]);
				}

				string leftToken;
				//process sub-tokens
				if (tokens.Length == 0)
				{
					throw new ArgumentException(string.Format("Incorrectly formatted formula; there does not appear to be an operand: '{0}'", token));
				}
				else if (tokens.Length == 1)
				{
					//no operator; process single operand 
					leftToken = tokens[0].Trim();
					if (!ParseOperand(leftToken, ref leftOperand, ref errorMessage))
					{
						throw new ApplicationException(errorMessage);
					}
					operand = leftOperand;

					returnValue = true;
				}
				else if (tokens.Length == 2)
				{
					//operator found; process dual operands with recursive call
					leftToken = tokens[0].Trim();
					if (!ParseToken(leftToken, ref leftOperand, ref errorMessage))
					{
						throw new ApplicationException(errorMessage);
					}

					string rightToken = tokens[1].Trim();
					if (!ParseToken(rightToken, ref rightOperand, ref errorMessage))
					{
						throw new ApplicationException(errorMessage);
					}

					//create operation from left/right operands and operator, and create OperandOperation
					//create OperationBinary which configures 2 OperandBase dictionary entries called Left, Right.
					OperationBase operation = new OperationBinary();
					operation.Operands["Left"] = leftOperand;
					operation.Operands["Right"] = rightOperand;
					operation.Operator = foundOperator;

					operand = new OperandOperation(operation);

					returnValue = true;
				}
				else if (tokens.Length > 2)
				{
					throw new ArgumentException(string.Format("Unexpected formula format: '{0}'", token));
				}
			}
            catch (Exception ex)
            {
                errorMessage = ex.Message;
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
                //throw;
            }
            return returnValue;
        }

        /// <summary>
        /// Parse token to identify whether it is a pre-parsing variable, a literal, or a cell-reference.
        /// </summary>
        /// <param name="token">string</param>
        /// <param name="operand">ref OperandBase</param>
        /// <param name="errorMessage">ref string</param>
        /// <returns>bool</returns>
        private bool ParseOperand(string token, ref OperandBase operand, ref string errorMessage)
        {
            bool returnValue = default;
			float numericResult = default;
            Sheet parentSheet = default;
            List<CategoryItem> criteria = default;
            string variableValue = default;

            try
            {
                //trap string literals
                if (token.Contains('\'') || token.Contains('"'))
                {
                    throw new ApplicationException(string.Format("String literals not supported: '{0}'", token));
                }
                //trap parentheses
                if (token.Contains('(') || token.Contains(')'))
                {
                    throw new ApplicationException(string.Format("Parentheses found after pre-processing: '{0}'", token));
                }

                token = token.Trim();

                if (token.StartsWith(ParsingVariableIndicator))
                {
                    //token is a parsing variable; look up value and process token as an expression

                    //look up parsing variable
                    if (!ParsingVariables.TryGetValue(token, out variableValue))
                    {
                        throw new ApplicationException(string.Format("Unable to look up variable value for variable name: '{0}'", token));
                    }

                    //parse expression and return operand
                    if (!ParseToken(variableValue, ref operand, ref errorMessage))
                    {
                        throw new ApplicationException(errorMessage);
                    }

                    returnValue = true;
                }
                else if (token.StartsWith(FunctionIndicator))
                {
                    //token is a function

                    //parse function and return operand
                    if (!ParseFunction(token, ref operand, ref errorMessage))
                    {
                        throw new ApplicationException(errorMessage);
                    }

                    returnValue = true;
                }
                else if (float.TryParse(token, out numericResult))
                {
                    //token is a numeric literal
                    //return literal operand with numeric value
                    operand = new OperandLiteral(numericResult);

                    returnValue = true;
                }
                else
                {
                    //token should be reference-operand
                    parentSheet = SettingsController<Settings>.Settings.Sheets.Find(s => s.Equals(Parent));
                    //parentSheet = SettingsController<Settings>.Settings.Sheets.Find(s => s.Name == this.Parent);
                    if (parentSheet == null)
                    {
                        throw new ApplicationException(string.Format("Unable to find sheet '{0}' for formula '{1}'.", Parent, Value));
                    }

                    criteria = parentSheet.GetCategoryItemsForAddressName(token, true);
                    if ((criteria == null) || (criteria.Count < 1))
                    {
                        throw new ApplicationException(string.Format("Unable to find category-item criteria in sheet '{0}' for formula '{1}'.", Parent, Value));
                    }

                    //store criteria
                    operand = new OperandReference(criteria);

                    returnValue = true;
                }
            }
            catch (Exception ex)
            {
                errorMessage = ex.Message;
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
                //throw;
            }
            return returnValue;
        }

        /// <summary>
        /// Parse the string token to get an operand of type Operation,
        /// whose operation is of type Function(?),
        /// and where the operator is the defined in the token.
        /// </summary>
        /// <param name="token">string</param>
        /// <param name="operand">ref OperandBase</param>
        /// <param name="errorMessage">ref string</param>
        /// <returns>bool</returns>
        private bool ParseFunction(string token, ref OperandBase operand, ref string errorMessage)
        {
            bool returnValue = default;
            bool isFoundOperator = default;
            OperatorBase foundOperator = default;
            int countOfLeftBraces = default;
            int countOfRightBraces = default;
            Regex regEx = default;
            Match match = default;
            string parametersToken = default;
			string[] parameterTokens = default;
            string functionNameToken = default;
            Dictionary<string, OperandBase> parameterDictionary = default;
            OperandBase parameterOperand = default;
            int parameterIndex = default;

            try
            {
                countOfLeftBraces = (from char ch in token
                                          where ch == LeftBrace
									 select ch).Count();
                countOfRightBraces = (from char ch in token
                                           where ch == RightBrace
									  select ch).Count();
                if ((countOfLeftBraces > 0) || (countOfRightBraces > 0))
                {
                    //braces found

                    if (countOfLeftBraces != countOfRightBraces)
                    {
                        //mismatched pair
                        throw new ApplicationException(string.Format("Mis-matched braces in token: '{0}'", token));
                    }

                    //get parameters

                    regEx = new Regex(RegExFindInnermostBracesAndContents);
                    match = regEx.Match(token);
                    if (string.IsNullOrEmpty(match.Value))
                    {
                        //pair did not match pattern
                        throw new ApplicationException(string.Format("Braces in token not formatted correctly: '{0}'", token));
                    }

                    parametersToken = match.Value.Replace(LeftBrace.ToString(), string.Empty).Replace(RightBrace.ToString(), string.Empty);
                    if (string.IsNullOrEmpty(parametersToken))
                    {
                        //pair did not contain an expression
                        throw new ApplicationException(string.Format("Value in parenthesis in token not formatted correctly: '{0}'", token));
                    }

                    parameterTokens = parametersToken.Split(FunctionParameterDelimiter);
                    if (parameterTokens.Length == 0)
                    {
                        //pair did not contain any values
                        throw new ApplicationException(string.Format("Value in parenthesis in token not formatted correctly: '{0}'", token));
                    }

                    //get function name

                    regEx = new Regex(RegExFindFunctionNameDelimitersAndContents);
                    match = regEx.Match(token);
                    if (string.IsNullOrEmpty(match.Value))
                    {
                        //pair did not match pattern
                        throw new ApplicationException(string.Format("Function name delimiters in token not formatted correctly: '{0}'", token));
                    }

                    functionNameToken = match.Value.Replace(FunctionIndicator, string.Empty).Replace(LeftBrace.ToString(), string.Empty);
                    if (string.IsNullOrEmpty(functionNameToken))
                    {
                        //pair did not contain an expression
                        throw new ApplicationException(string.Format("Function name in token not formatted correctly: '{0}'", token));
                    }

                    //look for operators, if any
                    foreach (OperatorBase op in functionOperators)
                    {
                        if (functionNameToken.Contains(op.Name, StringComparison.CurrentCultureIgnoreCase)) //Note: was ToUpper()
                        {
                            isFoundOperator = true;
                            foundOperator = op;

                            break;
                        }
                    }

                    //get function

                    if (isFoundOperator)
                    {
                        //build operation operand with function operation consisting of operator and each parameter token parsed into an operand.
                        parameterDictionary = [];
                        foreach (string parameterToken in parameterTokens)
                        {
                            if (!ParseToken(parameterToken, ref parameterOperand, ref errorMessage))
                            {
                                //error parsing operand token
                                throw new ApplicationException(string.Format("Unable to parse parameter '{0}' in function token '{1}'.", parameterToken, token));
                            }
                            parameterDictionary.Add(string.Format("Value{0}", parameterIndex++), parameterOperand);
                        }

                        //set operand to new OperandOperation of type OperationFunction using operator of type OperatorXXX (foundOperator)
                        operand = new OperandOperation(new OperationFunction(foundOperator, parameterDictionary));
                    }
                    else
                    {
                        throw new ApplicationException(string.Format("Unable to find function: '{0}'", token));
                    }
                }
                else
                {
                    throw new ApplicationException(string.Format("Unable to find braces for parameters: '{0}'", token));
                }

                returnValue = true;
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
                //throw;
            }
            return returnValue;
        }
        #endregion Private Methods
        #endregion Methods
    }
}
