﻿using System;
using System.ComponentModel;
using System.Runtime.Serialization;
using System.Reflection;
using Ssepan.Application.Core;

using Ssepan.Collections.Core;
using Ssepan.Utility.Core;

namespace MultiDimensionalSpreadsheetLibrary.Core
{
	//[Serializable()]
	//[DataContract(IsReference=true)]
	/// <summary>
	/// Category
	/// </summary>
	[DataContract(IsReference = true)]
    [KnownType(typeof(Category))]
    [TypeConverter(typeof(ExpandableObjectConverter))]
    public class Category :
        SettingsComponentBase,
        IDisposable,
        IEquatable<Category>,
        INotifyPropertyChanged
    {
        #region Declarations
        new private bool disposed;

        public enum SheetCategoryType
        {
            None,
            Filter,
            X,
            Y
        }
        #endregion Declarations

        #region constructors
        public Category()
        {
            try
            {
                if (Items != null)
                {
                    Items.ListChanged += Items_ListChanged;
                    Items.AddingNew += Items_AddingNew;
                }
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);

                throw;
            }
        }

        public Category(string name)
        {
            try
            {
                Name = name;

                if (Items != null)
                {
                    Items.ListChanged += Items_ListChanged;
                    Items.AddingNew += Items_AddingNew;
                }
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);

                throw;
            }
        }

        public Category(string name, SheetCategoryType sheetCategoryType)
        {
            try
            {
                Name = name;
                CategoryType = sheetCategoryType;

                if (Items != null)
                {
                    Items.ListChanged += Items_ListChanged;
                    Items.AddingNew += Items_AddingNew;
                }
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);

                throw;
            }
        }

        public Category(string name, CategoryItem categoryItem)
        {
            try
            {
                Name = name;
                Items.Add(categoryItem);

                if (Items != null)
                {
                    Items.ListChanged += Items_ListChanged;
                    Items.AddingNew += Items_AddingNew;
                }
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);

                throw;
            }
        }

        public Category(string name, CategoryItem categoryItem, SheetCategoryType sheetCategoryType)
        {
            try
            {
                Name = name;
                Items.Add(categoryItem);
                CategoryType = sheetCategoryType;

                if (Items != null)
                {
                    Items.ListChanged += Items_ListChanged;
                    Items.AddingNew += Items_AddingNew;
                }
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);

                throw;
            }
        }

        public Category(string name, OrderedEquatableBindingList<CategoryItem> categoryItems)
        {
            try
            {
                Name = name;
                Items = categoryItems;

                if (Items != null)
                {
                    Items.ListChanged += Items_ListChanged;
                    Items.AddingNew += Items_AddingNew;
                }
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);

                throw;
            }
        }

        public Category(string name, OrderedEquatableBindingList<CategoryItem> categoryItems, SheetCategoryType sheetCategoryType)
        {
            try
            {
                Name = name;
                Items = categoryItems;
                CategoryType = sheetCategoryType;

                if (Items != null)
                {
                    Items.ListChanged += Items_ListChanged;
                    Items.AddingNew += Items_AddingNew;
                }
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);

                throw;
            }
        }
        #endregion constructors

        #region IDisposable 
        ~Category()
        {
            Dispose(false);
        }

        new public void Dispose()
        {
            // dispose of the managed and unmanaged resources
            Dispose(true);

            // tell the GC that the Finalize process no longer needs
            // to be run for this object.
            GC.SuppressFinalize(this);
        }

        new protected virtual void Dispose(bool disposeManagedResources)
        {
            // process only if managed and unmanaged resources have
            // not been disposed of.
            if (!disposed)
            {
                //Resources not disposed
                if (disposeManagedResources)
                {
                    // dispose managed resources
                    if (Items != null)
                    {
                        Items.AddingNew -= Items_AddingNew;
                        Items.ListChanged -= Items_ListChanged;
                    }
                }
                // dispose unmanaged resources
                disposed = true;
            }
            else
            {
                //Resources already disposed
            }
        }
        #endregion IDisposable

        #region INotifyPropertyChanged support
        new public event PropertyChangedEventHandler PropertyChanged;

        new void OnPropertyChanged(string propertyName)
        {
            try
            {
				PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
			}
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);

                //throw;
            }
        }
        #endregion INotifyPropertyChanged support

        #region IEquatable<T>
        /// <summary>
        /// Compare property values of this object to another.
        /// </summary>
        /// <param name="other">Category</param>
        /// <returns>bool</returns>
        public bool Equals(Category other)
        {
			bool returnValue;
			try
            {
                if (this == other)
                {
                    returnValue = true;
                }
				else if (Name != other.Name)
				{
					returnValue = false;
				}
				else if (!Items.Equals(other.Items))
				{
					returnValue = false;
				}
				else if (CategoryType != other.CategoryType)
				{
					returnValue = false;
				}
				else if (SelectedItemIndex != other.SelectedItemIndex)
				{
					returnValue = false;
				}
				else
				{
					returnValue = true;
				}
			}
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);

                throw;
            }

            return returnValue;
        }
        #endregion IEquatable<T>

        #region ListChanged handlers
        void Items_ListChanged(object sender, ListChangedEventArgs e)
        {
            try
            {
                OnPropertyChanged(string.Format("Category.Items[{0}].{1}", e.NewIndex, e.PropertyDescriptor == null ? string.Empty : e.PropertyDescriptor.Name));
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);

                throw;
            }
        }
        #endregion ListChanged handlers

        #region AddingNew handlers
        void Items_AddingNew(object sender, AddingNewEventArgs e)
        {
            try
            {
                e.NewObject = new CategoryItem(CategoryItem.NewItemName, this);
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);

                throw;
            }
        }
        #endregion ListChanged handlers

        #region Properties

        #region Persisted Properties
        private string _Name = string.Empty;
        [DataMember]
        public string Name
        {
            get { return _Name; }
            set
            {
                _Name = value;
                OnPropertyChanged(nameof(Name));
            }
        }

        private OrderedEquatableBindingList<CategoryItem> _Items = [];
        [DataMember]
        public OrderedEquatableBindingList<CategoryItem> Items
        {
            get { return _Items; }
            set
            {
                if (Items != null)
                {
                    Items.ListChanged -= Items_ListChanged;
                    Items.AddingNew -= Items_AddingNew;
                }
                _Items = value;
                if (Items != null)
                {
                    Items.AddingNew += Items_AddingNew;
                    Items.ListChanged += Items_ListChanged;
                }
                OnPropertyChanged(nameof(Items));
            }
        }

        /// <summary>
        /// Indicates whether the category is assigned to a sheet category, and if so what type.
        /// </summary>
        private SheetCategoryType _CategoryType = SheetCategoryType.None;
        [DataMember]
        public SheetCategoryType CategoryType
        {
            get { return _CategoryType; }
            set
            {
                _CategoryType = value;
                OnPropertyChanged(nameof(CategoryType));

                if (value == SheetCategoryType.Filter)
                {
                    SelectedItemIndex = 0;
                }
                else
                {
                    SelectedItemIndex = -1;
                }
            }
        }

        private int _SelectedItemIndex = -1;
        /// <summary>
        /// Used when CategoryType == SheetCategoryType.Filter
        /// </summary>
        [DataMember]
        public int SelectedItemIndex
        {
            get { return _SelectedItemIndex; }
            set
            {
                _SelectedItemIndex = value;
                OnPropertyChanged(nameof(SelectedItemIndex));
            }
        }
        #endregion Persisted Properties
        #endregion Properties
    }
}
