﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using System.Reflection;
using Ssepan.Application.Core;

using Ssepan.Collections.Core;
using Ssepan.Utility.Core;

namespace MultiDimensionalSpreadsheetLibrary.Core
{
	/// <summary>
	/// Sheet
	/// </summary>
	//[Serializable()]
	[DataContract(IsReference=true)]
    [KnownType(typeof(Sheet))]
    [TypeConverter(typeof(ExpandableObjectConverter))]
    public class Sheet :
        SettingsComponentBase,
        IDisposable,
        IEquatable<Sheet>,
        INotifyPropertyChanged
    {
        #region Declarations
        public const string NewItemName = "(new sheet)";

        new private bool disposed;

        public readonly static char SheetAddressNameItemDelimiter = ':';
        public readonly static char SheetAddressNameItemComponentDelimiter = '.';

        public enum CellTypes
        {
            Empty,
            XCategory,
            YCategory,
            Value
        }

        public enum SheetDimensionChange
        {
            Increase,
            Decrease
        }
        #endregion Declarations

        #region constructors
        public Sheet()
        {
            try
            {
                if (_Cells != null)
                {
                    _Cells.ListChanged += Cells_ListChanged;
                }
                if (_Categories != null)
                {
                    _Categories.ListChanged += Categories_ListChanged;
                }
                if (_Formulae != null)
                {
                    _Formulae.ListChanged += Formulae_ListChanged;
                    _Formulae.AddingNew += Formulae_AddingNew;
                }
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);

                throw;
            }
        }

        public Sheet(string name) :
            this()
        {
            try
            {
                Name = name;

                //if (Cells != null)
                //{
                //    Cells.ListChanged += new ListChangedEventHandler(Cells_ListChanged);
                //}
                //if (Categories != null)
                //{
                //    Categories.ListChanged += new ListChangedEventHandler(Categories_ListChanged);
                //}
                //if (Formulae != null)
                //{
                //    Formulae.ListChanged += new ListChangedEventHandler(Formulae_ListChanged);
                //    Formulae.AddingNew += new AddingNewEventHandler(Formulae_AddingNew);
                //}
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);

                throw;
            }
        }
        #endregion constructors

        #region IDisposable 
        ~Sheet()
        {
            Dispose(false);
        }

        new public void Dispose()
        {
            // dispose of the managed and unmanaged resources
            Dispose(true);

            // tell the GC that the Finalize process no longer needs
            // to be run for this object.
            GC.SuppressFinalize(this);
        }

        new protected virtual void Dispose(bool disposeManagedResources)
        {
            // process only if managed and unmanaged resources have
            // not been disposed of.
            if (!disposed)
            {
                //Resources not disposed
                if (disposeManagedResources)
                {
                    // dispose managed resources
                    if (Cells != null)
                    {
                        Cells.ListChanged -= Cells_ListChanged;
                    }
                    if (Categories != null)
                    {
                        Categories.ListChanged -= Categories_ListChanged;
                    }
                    if (Formulae != null)
                    {
                        Formulae.ListChanged -= Formulae_ListChanged;
                        Formulae.AddingNew -= Formulae_AddingNew;
                    }
                }
                // dispose unmanaged resources
                disposed = true;
            }
            else
            {
                //Resources already disposed
            }
        }
        #endregion IDisposable

        #region INotifyPropertyChanged support
        new public event PropertyChangedEventHandler PropertyChanged;

        new void OnPropertyChanged(string propertyName)
        {
            try
            {
				PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
			}
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);

                //throw;
            }
        }
        #endregion INotifyPropertyChanged support

        #region IEquatable<Sheet>
        /// <summary>
        /// Compare property values of this object to another.
        /// </summary>
        /// <param name="other">Sheet</param>
        /// <returns>bool</returns>
        public bool Equals(Sheet other)
        {
			bool  returnValue;
			try
            {
                if (this == other)
                {
                    returnValue = true;
                }
				else if (Name != other.Name)
				{
					returnValue = false;
				}
				else if (!Cells.Equals(other.Cells))
				{
					returnValue = false;
				}
				else if (!Categories.Equals(other.Categories))
				{
					returnValue = false;
				}
				else if (!Formulae.Equals(other.Formulae))
				{
					returnValue = false;
				}
				else
				{
					returnValue = true;
				}
			}
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);

                throw;
            }

            return returnValue;
        }
        #endregion IEquatable<Sheet>

        #region ListChanged handlers
        void Cells_ListChanged(object sender, ListChangedEventArgs e)
        {
            try
            {
                OnPropertyChanged(string.Format("Sheet.Cells[{0}].{1}", e.NewIndex, e.PropertyDescriptor == null ? string.Empty : e.PropertyDescriptor.Name));
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);

                throw;
            }
        }

        void Categories_ListChanged(object sender, ListChangedEventArgs e)
        {
            try
            {
                OnPropertyChanged(string.Format("Sheet.Categories[{0}].{1}", e.NewIndex, e.PropertyDescriptor == null ? string.Empty : e.PropertyDescriptor.Name));
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);

                throw;
            }
        }

        void Formulae_ListChanged(object sender, ListChangedEventArgs e)
        {
            try
            {
                OnPropertyChanged(string.Format("Sheet.Formulae[{0}].{1}", e.NewIndex, e.PropertyDescriptor == null ? string.Empty : e.PropertyDescriptor.Name));
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);

                throw;
            }
        }
        #endregion ListChanged handlers

        #region AddingNew handlers
        void Formulae_AddingNew(object sender, AddingNewEventArgs e)
        {
            try
            {
                e.NewObject = new Formula(Formula.NewItemName, this);
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);

                throw;
            }
        }
        #endregion ListChanged handlers

        #region Properties
        #region NonPersisted Properties
        /// <summary>
        /// Grid Column Y (vertical) offset of Y (vertical) category items and values. Offset by number of X (horizontal) categories from top.
        /// </summary>
        [XmlIgnore]
        public int YOffset
        {
            get { return (from sheetCategory in CategoryX() select sheetCategory).Count(); }
        }

        /// <summary>
        /// Grid Column X (horizontal) offset of X (horizontal) category items and values. Offset by number of Y (vertical) categories from left.
        /// </summary>
        [XmlIgnore]
        public int XOffset
        {
            get { return (from sheetCategory in CategoryY() select sheetCategory).Count(); }
        }

        /// <summary>
        /// Sum of X (horizontal) offset and aggregate product of X (horizontal) category item counts.
        /// </summary>
        [XmlIgnore]
        public int ColumnCount
        {
            get
            {
                if (CategoryX().Count == 0)
                {
                    return XOffset;
                }
                else
                {
                    return XOffset + (from sheetCategory in CategoryX() select sheetCategory.Items.Count).Aggregate(1, (seed, n) => seed * n);
                }
            }
        }

        /// <summary>
        /// Sum of Y (vertical) offset and aggregate product of Y (vertical) category item counts.
        /// </summary>
        [XmlIgnore]
        public int RowCount
        {
            get
            {
                if (CategoryY().Count == 0)
                {
                    return YOffset;
                }
                else
                {
                    return YOffset + (from sheetCategory in CategoryY() select sheetCategory.Items.Count).Aggregate(1, (seed, n) => seed * n) ;
                }
            }
        }

        /// <summary>
        /// Color of Background in empty cells.
        /// </summary>
        [XmlIgnore]
        public static Color ColorEmptyBackground
        {
            get { return SystemColors.ControlDark; }
        }

        /// <summary>
        /// Color of foreground in empty cells.
        /// </summary>
        [XmlIgnore]
        public static Color ColorEmptyForeground
        {
            get { return SystemColors.ControlText; }
        }

        /// <summary>
        /// Color of Background in category item cells.
        /// </summary>
        [XmlIgnore]
        public static Color ColorCategoryItemBackground
        {
            get { return SystemColors.Control; }
        }

        /// <summary>
        /// Color of foreground in category item cells.
        /// </summary>
        [XmlIgnore]
        public static Color ColorCategoryItemForeground
        {
            get { return SystemColors.ControlText; }
        }

        /// <summary>
        /// Color of background in value cells.
        /// </summary>
        [XmlIgnore]
        public static Color ColorValueBackground
        {
            get { return SystemColors.Window; }
        }

        /// <summary>
        /// Color of foreground in value cells.
        /// </summary>
        [XmlIgnore]
        public static Color ColorValueForeground
        {
            get { return SystemColors.ControlText; }
        }
        #endregion NonPersisted Properties

        #region Persisted Properties
        private string _Name = string.Empty;
        [DataMember]
        public string Name
        {
            get { return _Name; }
            set
            {
                _Name = value;
                OnPropertyChanged(nameof(Name));
            }
        }

        private EquatableBindingList<SheetCell> _Cells = [];
        [DataMember]
        public EquatableBindingList<SheetCell> Cells
        {
            get { return _Cells; }
            set
            {
                if (_Cells != null)
                {
                    _Cells.ListChanged -= Cells_ListChanged;
                }
                _Cells = value;
                if (_Cells != null)
                {
                    _Cells.ListChanged += Cells_ListChanged;
                }
                OnPropertyChanged(nameof(Cells));
            }
        }

        private OrderedEquatableBindingList<Category> _Categories = [];
        [DataMember]
        public OrderedEquatableBindingList<Category> Categories
        {
            get { return _Categories; }
            set
            {
                if (_Categories != null)
                {
                    _Categories.ListChanged -= Categories_ListChanged;
                }
                _Categories = value;
                if (_Categories != null)
                {
                    _Categories.ListChanged += Categories_ListChanged;
                }
                OnPropertyChanged(nameof(Categories));
            }
        }

        private OrderedEquatableBindingList<Formula> _Formulae = [];
        [DataMember]
        public OrderedEquatableBindingList<Formula> Formulae
        {
            get { return _Formulae; }
            set
            {
                if (_Formulae != null)
                {
                    _Formulae.ListChanged -= Formulae_ListChanged;
                    _Formulae.AddingNew -= Formulae_AddingNew;
                }
                _Formulae = value;
                if (_Formulae != null)
                {
                    _Formulae.ListChanged += Formulae_ListChanged;
                    _Formulae.AddingNew += Formulae_AddingNew;
                }
                OnPropertyChanged(nameof(Formulae));
            }
        }

        public List<Category> CategoryFilters()
        {
			var result = from sheetCategory in Categories
                          where sheetCategory.CategoryType == Category.SheetCategoryType.Filter
                          select sheetCategory;
			return result.ToList();
		}

        public List<Category> CategoryX()
        {
			var result = from sheetCategory in Categories
                        where sheetCategory.CategoryType == Category.SheetCategoryType.X
                        select sheetCategory ;
			return result.ToList();
		}

        public List<Category> CategoryY()
        {
			var result = from sheetCategory in Categories
                          where sheetCategory.CategoryType == Category.SheetCategoryType.Y
                          select sheetCategory;
			return result.ToList();
		}
        #endregion Persisted Properties
        #endregion Properties

        #region Public Methods
        /// <summary>
        /// Takes row, column indices, and returns enum indicating in which range the cell resides
        /// </summary>
        /// <param name="rowIndex">int</param>
        /// <param name="columnIndex">int</param>
        /// <returns>CellTypes</returns>
        public CellTypes GetCellTypeAtCoordinates
        (
            int rowIndex,
            int columnIndex
        )
        {
			CellTypes returnValue;
			try
            {
                if ((rowIndex >= 0) && (rowIndex < YOffset))
                {
                    //Empty or X categories
                    if ((columnIndex >= 0) && (columnIndex < XOffset))
                    {
                        returnValue = CellTypes.Empty;
                    }
                    else if ((columnIndex >= XOffset) && (columnIndex < ColumnCount))
                    {
                        returnValue = CellTypes.XCategory;
                    }
                    else
                    {
                        throw new IndexOutOfRangeException(string.Format("Invalid column index ({0}). Valid range is ({1} .. {2}).", columnIndex, 0, ColumnCount));
                    }
                }
                else if ((rowIndex >= YOffset) && (rowIndex < RowCount))
                {
                    //Y categories or Values
                    if ((columnIndex >= 0) && (columnIndex < XOffset))
                    {
                        returnValue = CellTypes.YCategory;
                    }
                    else if ((columnIndex >= XOffset) && (columnIndex < ColumnCount))
                    {
                        returnValue = CellTypes.Value;
                    }
                    else
                    {
                        throw new IndexOutOfRangeException(string.Format("Invalid column index ({0}). Valid range is ({1} .. {2}).", columnIndex, 0, ColumnCount));
                    }
                }
                else if ((YOffset == 0) && (XOffset == 0))
                {
                    //no X or Y categories defined
                    returnValue = CellTypes.Empty;
                }
                else
                {
                    throw new IndexOutOfRangeException(string.Format("Invalid row index ({0}). Valid range is ({1} .. {2}).", rowIndex, 0, RowCount));
                }
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);

                throw;
            }
            return returnValue;
        }

        /// <summary>
        /// Takes row / column indices, and returns integer indicating a category-item.
        /// Also returns an 'out' parameter indicating if the category item is the first instance displayed
        ///  or a repeated item (for use in drawing an interface).
        /// </summary>
        /// <param name="rowIndex">int</param>
        /// <param name="columnIndex">int</param>
        /// <param name="isItemFirstIndex">out bool</param>
        /// <returns>int</returns>
        public int GetCategoryItemIndexAtCoordinates
        (
            int rowIndex,
            int columnIndex,
            out bool isItemFirstIndex
        )
        {
            int returnValue = -1;
            int itemFirstIndex = -1;
			const CellTypes cellType = default;
            int categoryItemsCount =  -1;
            int subCategoriesItemsCount =  -1;
            isItemFirstIndex = default;

            try
            {
                //identify cell type from range given by row/column
                switch (GetCellTypeAtCoordinates(rowIndex, columnIndex))
                {
					case CellTypes.Empty:
						throw new ArgumentOutOfRangeException(string.Format("Cell at coordinates ({0}, {1}) is not a Category Item; it is of type {2}.", rowIndex, columnIndex, cellType.ToString()));

						//break;

					case CellTypes.XCategory:
                        {
                            //if x category range, identify category-item @ x, y 
                            categoryItemsCount = CategoryX().Where((_, index) => index == rowIndex).Select(c => c.Items.Count).Aggregate(1, (seed, n) => seed * n);
                            subCategoriesItemsCount = CategoryX().Where((_, index) => index > rowIndex).Select(c => c.Items.Count).Aggregate(1, (seed, n) => seed * n);

                            returnValue = (columnIndex - XOffset) / subCategoriesItemsCount % categoryItemsCount;

                            itemFirstIndex = (columnIndex - XOffset) % subCategoriesItemsCount;
                            isItemFirstIndex = itemFirstIndex == 0;

                            break;
                        }
                    case CellTypes.YCategory:
                        {
                            categoryItemsCount = CategoryY().Where((_, index) => index == columnIndex).Select(c => c.Items.Count).Aggregate(1, (seed, n) => seed * n);
                            subCategoriesItemsCount = CategoryY().Where((_, index) => index > columnIndex).Select(c => c.Items.Count).Aggregate(1, (seed, n) => seed * n);

                            returnValue = (rowIndex - YOffset) / subCategoriesItemsCount % categoryItemsCount;

                            itemFirstIndex = (rowIndex - YOffset) % subCategoriesItemsCount;
                            isItemFirstIndex = itemFirstIndex == 0;

                            break;
                        }
					case CellTypes.Value:
						throw new ArgumentOutOfRangeException(string.Format("Cell at coordinates ({0}, {1}) is not a Category Item; it is of type {2}.", rowIndex, columnIndex, cellType.ToString()));

						//break;
				}
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);

                throw;
            }
            return returnValue;
        }

        /// <summary>
        /// Takes row / column indices, and returns string representing the name of a category-item.
        /// Also takes boolean indicating whether to return blank instead of repeating the first instance of the item.
        /// </summary>
        /// <param name="rowIndex">int</param>
        /// <param name="columnIndex">int</param>
        /// <param name="skipIfNotFirstItemInstance">bool</param>
        /// <returns>string</returns>
        public string GetCategoryItemNameAtCoordinates
        (
            int rowIndex,
            int columnIndex,
            bool skipIfNotFirstItemInstance
        )
        {
            string returnValue = default;
			const CellTypes cellType = default;

            try
            {
                //identify cell type from range given by row/column
                switch (GetCellTypeAtCoordinates(rowIndex, columnIndex))
                {
					case CellTypes.Empty:
						throw new ArgumentOutOfRangeException(string.Format("Cell at coordinates ({0}, {1}) is not a Category Item; it is of type {2}.", rowIndex, columnIndex, cellType.ToString()));

						//break;

					case CellTypes.XCategory:
                        {
                            //if x category range, identify category-item @ x, y 
                            int itemIndex = -1;

							itemIndex = GetCategoryItemIndexAtCoordinates(rowIndex, columnIndex, out bool isItemFirstIndex);

							if (isItemFirstIndex || !skipIfNotFirstItemInstance)
                            {
                                returnValue = CategoryX()[rowIndex].Items[itemIndex].Name;
                            }
                            else
                            {
                                returnValue = "";
                            }

                            break;
                        }
                    case CellTypes.YCategory:
                        {
                            //if y category range, identify category-item @ x, y 
                            int itemIndex = -1;

							itemIndex = GetCategoryItemIndexAtCoordinates(rowIndex, columnIndex, out bool isItemFirstIndex);

							if (isItemFirstIndex || !skipIfNotFirstItemInstance)
                            {
                                returnValue = CategoryY()[columnIndex].Items[itemIndex].Name;
                            }
                            else
                            {
                                returnValue = "";
                            }

                            break;
                        }
					case CellTypes.Value:
						throw new ArgumentOutOfRangeException(string.Format("Cell at coordinates ({0}, {1}) is not a Category Item; it is of type {2}.", rowIndex, columnIndex, cellType.ToString()));

						//break;
				}
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);

                throw;
            }
            return returnValue;
        }

        /// <summary>
        /// Takes index into Filter category and returns index of Category's selected Item.
        /// </summary>
        /// <param name="filterIndex">int</param>
        /// <returns>int</returns>
        public int GetCategoryItemIndexAtFilterIndex
        (
            int filterIndex
        )
        {
            int returnValue = -1;
			try
			{
				List<Category> filters = CategoryFilters();
				if ((filterIndex > -1) && (filterIndex < filters.Count))
                {
					Category category = filters[filterIndex];
					returnValue = category.SelectedItemIndex;
                }
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);

                throw;
            }
            return returnValue;
        }

        /// <summary>
        /// Takes row / column indices of value cell, and returns list of category-items.
        /// </summary>
        /// <param name="rowIndex">int</param>
        /// <param name="columnIndex">int</param>
        /// <returns>List<CategoryItem></returns>
        public List<CategoryItem> GetCategoryItemsAtValueCoordinates
        (
            int rowIndex,
            int columnIndex
        )
        {
            List<CategoryItem> returnValue = [];
			const CellTypes cellType = default;
			try
			{
				//identify cell type from range given by row/column
				switch (GetCellTypeAtCoordinates(rowIndex, columnIndex))
				{
					case CellTypes.Empty:
						//if empty range, ignore
						throw new ArgumentOutOfRangeException(string.Format("Cell at coordinates ({0}, {1}) is not a Value; it is of type {2}.", rowIndex, columnIndex, cellType.ToString()));

					case CellTypes.XCategory:
						//if x category range, ignore
						throw new ArgumentOutOfRangeException(string.Format("Cell at coordinates ({0}, {1}) is not a Value; it is of type {2}.", rowIndex, columnIndex, cellType.ToString()));

					case CellTypes.YCategory:
						//if y category range, ignore
						throw new ArgumentOutOfRangeException(string.Format("Cell at coordinates ({0}, {1}) is not a Value; it is of type {2}.", rowIndex, columnIndex, cellType.ToString()));

					case CellTypes.Value:
						int categoryItemIndex;
						//if value range, identify category-item(s) for x, y 

						//get Filter items
						for (int filterIndex = 0; filterIndex < CategoryFilters().Count; filterIndex++)
						{
							categoryItemIndex = GetCategoryItemIndexAtFilterIndex(filterIndex);
							if (categoryItemIndex != -1)
							{
								returnValue.Add(CategoryFilters()[filterIndex].Items[categoryItemIndex]);
							}
						}
						bool isItemFirstIndex;
						//get X items
						for (int yIndex = 0; yIndex < YOffset; yIndex++)
						{
							categoryItemIndex = GetCategoryItemIndexAtCoordinates(yIndex, columnIndex, out isItemFirstIndex);
							returnValue.Add(CategoryX()[yIndex].Items[categoryItemIndex]);
						}
						//get Y items
						for (int xIndex = 0; xIndex < XOffset; xIndex++)
						{
							categoryItemIndex = GetCategoryItemIndexAtCoordinates(rowIndex, xIndex, out isItemFirstIndex);
							returnValue.Add(CategoryY()[xIndex].Items[categoryItemIndex]);
						}

						break;
				}
			}
			catch (Exception ex)
			{
				Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);

				throw;
			}
			return returnValue;
        }

        /// <summary>
        /// Takes a string representing a named cell (or cell-range) address, and returns a List of CategoryItem (which can be used to search for the cells represented).
        /// </summary>
        /// <param name="addressName">string</param>
        /// <param name="exactMatch">bool</param>
        /// <returns>List<CategoryItem></returns>
        public List<CategoryItem> GetCategoryItemsForAddressName
        (
            string addressName,
            bool exactMatch
        )
        {
			string categoryName = default;
            string itemName = default;

			List<CategoryItem> returnValue;
			try
			{
				returnValue = [];

				//validate address
				if (string.IsNullOrEmpty(addressName))
				{
					throw new ArgumentException(string.Format("Address Name was empty: '{0}'", addressName));
				}

				//separate item names
				string[] addressNameItems = addressName.Split(SheetAddressNameItemDelimiter);

				if (addressNameItems == null || addressNameItems.Length == 0)
				{
					throw new ArgumentException(string.Format("Address Name was empty: '{0}'", addressName));
				}

				foreach (string addressNameItem in addressNameItems)
				{
					if (addressNameItem?.Length == 0)
					{
						throw new ArgumentException(string.Format("Address Name component was empty. Address Name: '{0}'", addressName));
					}

					//separate item name components
					string[] addressNameItemComponents = addressNameItem.Split(SheetAddressNameItemComponentDelimiter);

					if ((addressNameItemComponents == null) || (addressNameItemComponents.Length < 1) || (addressNameItemComponents.Length > 2))
					{
						throw new ArgumentException(string.Format("Address Name Item was incorrectly formatted: '{0}'", addressNameItem));
					}

					if (addressNameItemComponents.Length == 1)
					{
						categoryName = default;
						itemName = addressNameItemComponents[0];
					}
					else if (addressNameItemComponents.Length == 2)
					{
						categoryName = addressNameItemComponents[0];
						itemName = addressNameItemComponents[1];
					}

					//find item(s)
					//append to List on each iteration; list should have 1 found item each time.
					List<CategoryItem> result = GetCategoryItemsWhereCategoryAndOrItemsNamesMatch(categoryName, itemName);
					if (result.Count == 0)
					{
						throw new ApplicationException(string.Format("No category items found for category '{0}' and item '{1}'", categoryName, itemName));
					}
					if (result.Count == 1)
					{
						returnValue.AddRange(result);
					}
					if (result.Count > 1)
					{
						if (exactMatch)
						{
							throw new ApplicationException(string.Format("{0} category items found for category '{1}' and item '{2}'", result.Count, categoryName, itemName));
						}
						else
						{
							returnValue.AddRange(result);
						}
					}
				}
			}
			catch (Exception ex)
			{
				Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);

				throw;
			}
			return returnValue;
        }

        /// <summary>
        /// Takes row / column indices of value cell, and returns index of a single cell object.
        /// </summary>
        /// <param name="rowIndex">int</param>
        /// <param name="columnIndex">int</param>
        /// <returns>int</returns>
        public int GetValueIndexAtCoordinates
        (
            int rowIndex,
            int columnIndex
        )
        {
            int returnValue = 0; // -1; 
			const CellTypes cellType = default;
			//return returnValue;
			try
			{
                //identify cell type from range given by row/column
                switch (GetCellTypeAtCoordinates(rowIndex, columnIndex))
                {
					case CellTypes.Empty:
						//if empty range, ignore
						throw new ArgumentOutOfRangeException(string.Format("Cell at coordinates ({0}, {1}) is not a Value; it is of type {2}.", rowIndex, columnIndex, cellType.ToString()));

					case CellTypes.XCategory:
						//if x category range, ignore
						throw new ArgumentOutOfRangeException(string.Format("Cell at coordinates ({0}, {1}) is not a Value; it is of type {2}.", rowIndex, columnIndex, cellType.ToString()));

					case CellTypes.YCategory:
						//if y category range, ignore
						throw new ArgumentOutOfRangeException(string.Format("Cell at coordinates ({0}, {1}) is not a Value; it is of type {2}.", rowIndex, columnIndex, cellType.ToString()));

					case CellTypes.Value:
						//if value range, identify category-item(s) for x, y 
						List<CategoryItem> valueCellCategoryItems = GetCategoryItemsAtValueCoordinates(rowIndex, columnIndex);
						if (valueCellCategoryItems.Count < 1)
						{
							returnValue = -1;
						}
						else
						{
							//perform lookup in cells for matching category and item
							List<SheetCell> cells = GetCellsWhereAllCellCategoryItemsMatchAnyInGivenSearch([.. Cells], valueCellCategoryItems);
							if (cells.Count > 1)
							{
								//too many matches; exact match not found
								//This may happen if a dimension  (category) was  recently removed from the sheet,
								// but it should have been dealt with at that time.
								//TODO:we could try to deal with it here, and try to identify and eliminate cells 
								// with categories not present in sheet categories.
								returnValue = -1;
							}
							else if (cells.Count < 1)
							{
								//no matches found; is missing, need to add new cell
								//Add sheet cells with references to sheet-categories
								//This may happen if a dimension (category) was  recently added to the sheet,
								// but was deferred until cell was needed here.

								SheetCell cell = new("", valueCellCategoryItems.ToEquatableBindingList());
								Cells.Add(cell);

								returnValue = Cells.IndexOf(cell);
							}
							else //cells.Count == 1
							{
								//identify actual index in Cells collection
								returnValue = Cells.IndexOf(cells[0]);
							}
						}
						break;
				}
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);

                throw;
            }
            return returnValue;
        }

        /// <summary>
        /// Set selected Item index of filter Category Item.
        /// </summary>
        /// <param name="categoryItem">CategoryItem</param>
        public void SetFilterCategoryItem
        (
            CategoryItem categoryItem
        )
        {
            Category category = default;

            try
            {
                category = Categories.Find(c => c.Equals(categoryItem.Parent));
                //category = this.Categories.Find(c => c.Name == categoryItem.Parent);
                if (category == null)
                {
                    throw new ArgumentException(string.Format("Category not found: '{0}'", categoryItem.Parent));
                }

                if (category.Items.Count > 0)
                {
                    category.SelectedItemIndex = category.Items.IndexOf(categoryItem);
                }
                else
                {
                    throw new ApplicationException(string.Format("Category '{0}' has no items; cannot set a selected Item.", category.Name));
                }
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);

                throw;
            }
        }

        /// <summary>
        /// When assigning a SheetCategoryType to a Category, if the source or destination SheetCategoryType
        ///  is None, then the assignment will also trigger a change of sheet dimension.
        /// If moving category FROM None, this is an increase in dimension, and we must add one category-item from category to all cells.
        /// If moving category TO None, this is a decrease in dimension, and we must remove one category-item from category from matching cells, and delete all cells containing category's other items.
        /// </summary>
        /// <param name="categoryToBeAssigned">Category that had it's CategoryType assignment changed.</param>
        /// <param name="categoryItemDefault">Default CategoryItem assigned to existing cells if adding a new dimension,
        /// or used to choose cells to keep (among those with different CategoryItems for the category) if removing a dimension</param>
        /// <param name="sheetDimensionChange">Increase or decrease of sheet dimension, as defined by Sheet.Categories with Category.CategoryType not equal to None.</param>
        public void ChangeSheetDimensionByCategory
        (
            Category categoryToBeAssigned, 
            CategoryItem categoryItemDefault,
            SheetDimensionChange sheetDimensionChange
        )
        {
            try
            {
                switch (sheetDimensionChange)
                {
                    case SheetDimensionChange.Increase:
                        {
                            //Increase dimension of sheet

                            //must add one category-item from category to all cells
                            ProcessMatchingCells
                            (
                                () => [.. Cells],
                                c => c.CategoryItems.Add(categoryItemDefault)
                            );

                            break;
                        }
                    case SheetDimensionChange.Decrease:
                        {
                            //Decrease dimension of sheet

                            //remove category-item from matching cells
                            //must remove matching category-item in removed category from containing cells 
                            ProcessMatchingCells
                            (
                                () => GetCellsWhereAllSearchCategoryItemsMatchAnyInGivenCell([.. Cells], [categoryItemDefault]),
                                c => c.CategoryItems.Remove(categoryItemDefault)
                            );

							//remove non-matching cells
							//must delete all cells containing category's other items
							_ = ProcessMatchingCells
							(
								() => GetCellsWhereAllListNotMatchAnyCategoryItem([.. Cells], [categoryItemDefault]),
								Cells.Remove
							);

                            break;
                        }
                }
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
            }
        }

        /// <summary>
        /// Add or remove a Category to one of the SheetCategoryTypes.
        /// If the source or destination SheetCategoryType is None, then the assignment will also trigger
        ///  a change of sheet dimension.
        /// </summary>
        /// <param name="categoryNameToBeAssigned">Name of Category to have it's CategoryType assignment changed.</param>
        /// <param name="categoryTypeDestination">CategoryType to be assigned to named Category.</param>
        /// <param name="categoryItemDefault">See <see cref="ChangeSheetDimensionByCategory"/>.
        /// Ignored (and may be null) if there is no change of dimension</param>
        public void AssignCategory
        (
            string categoryNameToBeAssigned,
            Category.SheetCategoryType categoryTypeDestination,
            CategoryItem categoryItemDefault
        )
        {
            Category categoryToBeAssigned = default;

            try
            {
                categoryToBeAssigned = Categories.Find(c => c.Name == categoryNameToBeAssigned);

                if (categoryToBeAssigned.Items.Count > 0)
                {
                    if (categoryToBeAssigned.CategoryType != categoryTypeDestination)
                    {
                        //move it
                        if (categoryTypeDestination == Category.SheetCategoryType.None)
                        {
                            //move TO None
                            categoryToBeAssigned.CategoryType = categoryTypeDestination;

                             //Decrease dimension of sheet
                            ChangeSheetDimensionByCategory(categoryToBeAssigned, categoryItemDefault, SheetDimensionChange.Decrease);
                        }
                        else if (categoryToBeAssigned.CategoryType == Category.SheetCategoryType.None)
                        {
                            //move FROM None
                            categoryToBeAssigned.CategoryType = categoryTypeDestination;

                            //Increase dimension of sheet
                            ChangeSheetDimensionByCategory(categoryToBeAssigned, categoryItemDefault, SheetDimensionChange.Increase);
                        }
                        else
                        {
                            //simple move among X / Y / Filter
                            categoryToBeAssigned.CategoryType = categoryTypeDestination;

                            //dimension of sheet unchanged
                        }
                    }
                    else
                    {
                        //source and destination are the same; take no action.
                    }
                }
                else
                {
                    throw new ApplicationException(string.Format("Category '{0}' has no items; it cannot be moved to '{1}'.", categoryToBeAssigned.Name, categoryTypeDestination.ToString()));
                }
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);

                throw;
            }
        }
        #endregion Public Methods

        #region Private Methods
        /// <summary>
        /// Take category name and / or item name, and return list of category items.
        /// </summary>
        /// <param name="categoryName">string</param>
        /// <param name="itemName">string</param>
        /// <returns>List<CategoryItem></returns>
        public List<CategoryItem> GetCategoryItemsWhereCategoryAndOrItemsNamesMatch
        (
            string categoryName,
            string itemName
        )
        {
            List<CategoryItem> returnValue = default;
            string categoryFilter = string.Empty;
            string itemFilter = string.Empty;

            try
            {
                if (string.IsNullOrEmpty(categoryName))
                {
                    categoryFilter = "*";
                }
                else
                {
                    categoryFilter = categoryName;
                }

                if (string.IsNullOrEmpty(itemName))
                {
                    itemFilter = "*";
                }
                else
                {
                    itemFilter = itemName;
                }

                returnValue =
                    (from c in Categories
                     where c.Name.Like(categoryFilter)
                     from i in c.Items
                     where i.Name.Like(itemFilter)
                     select i).ToList();
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);

                throw;
            }
            return returnValue;
        }
        #endregion Private Methods

        #region Static Methods
        /// <summary>
        /// Takes a list of Category Items and formats them as string.
        /// </summary>
        /// <param name="criteria">List<CategoryItem></param>
        /// <returns>string</returns>
        public static string GetFormattedCellAddress
        (
            List<CategoryItem> criteria
        )
        {
            string returnValue = default;
            try
            {
                returnValue =
					string.Join
                    (
						SheetAddressNameItemDelimiter.ToString(),
                        (
                            from categoryItem in criteria
                            select string.Format
                            (
                                "{0}{1}{2}",
                                categoryItem.Parent,
								SheetAddressNameItemComponentDelimiter,
                                categoryItem.Name
                            )
                        ).ToArray()
                    );
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);

                throw;
            }
            return returnValue;
        }

        //re-usable functions to return different cell search results
        #region Cell Searching Components
        /// <summary>
        /// Takes a list of cells and a list of category items, and returns a list of cells.
        /// It lets the caller decide how to use them,
        ///  and makes no assumption about how strict a match the caller intended.
        /// Get cells where all cell category items match any entry of a given search's category items.
        /// Typically used to match any??? sheet cells whose category item criteria contain all category item criteria defined at a
        /// particular set of sheet coordinates.
        /// </summary>
        /// <param name="cells">List<SheetCell></param>
        /// <param name="searchCategoryItems">List<CategoryItem></param>
        /// <returns>List<SheetCell></returns>
        public static List<SheetCell> GetCellsWhereAllCellCategoryItemsMatchAnyInGivenSearch
        (
            List<SheetCell> cells,
            List<CategoryItem> searchCategoryItems
        )
        {
            List<SheetCell> returnValue = [];
            try
            {
                returnValue =
                    (from cell in cells
                    where cell.CategoryItems.All(cellCategoryItem => searchCategoryItems.Any(searchCategoryItem => searchCategoryItem == cellCategoryItem))
                    select cell).ToList();
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);

                throw;
            }
            return returnValue;
        }

        /// <summary>
        /// Takes a list of cells and a list of category items, and returns a list of cells.
        /// It lets the caller decide how to use them,
        ///  and makes no assumption about how strict a match the caller intended.
        /// Get cells where all search criteria category items match any of a given cell's category items.
        /// Typically used by formulas and for finding cells with simlar categories but different category items in one category.
        /// </summary>
        /// <param name="cells">List<SheetCell></param>
        /// <param name="searchCategoryItems">List<CategoryItem></param>
        /// <returns>List<SheetCell></returns>
        public static List<SheetCell> GetCellsWhereAllSearchCategoryItemsMatchAnyInGivenCell
        (
            List<SheetCell> cells,
            List<CategoryItem> searchCategoryItems
        )
        {
            List<SheetCell> returnValue = [];
            try
            {
                returnValue =
                    (from cell in cells
                    where searchCategoryItems.All(searchCategoryItem => cell.CategoryItems.Any(cellCategoryItem => cellCategoryItem == searchCategoryItem))
                    select cell).ToList();
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);

                throw;
            }
            return returnValue;
        }

        /// <summary>
        /// Takes a list of sheet cells and a list of category items, and returns a list of cell objects.
        /// It lets the caller decide how to use them,
        ///  and makes no assumption about how strict a match the caller intended.
        /// Get all cells where *all* entries in the passed category-item list match *any* entry in a given cell's category-item list
        ///  where the category is a match, but the child items are NOT the matching item.
        /// Used for partial match on a subset of the category-items.
        /// </summary>
        /// <param name="sheetCells">List<SheetCell></param>
        /// <param name="sheetCategoryItems">List<CategoryItem></param>
        /// <returns>List<SheetCell></returns>
        public static List<SheetCell> GetCellsWhereAllListNotMatchAnyCategoryItem
        (
            List<SheetCell> sheetCells,
            List<CategoryItem> sheetCategoryItems
        )
        {
            List<SheetCell> returnValue = [];
            try
            {
                returnValue =
                    (from cell in sheetCells
                    where sheetCategoryItems.All(ci1 => cell.CategoryItems.Any(ci2 => (ci2.Parent == ci1.Parent) && (ci2.Name != ci1.Name)))
                    select cell).ToList();
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);

                throw;
            }
            return returnValue;
        }
        #endregion Cell Searching Components

        //re-usable functions to apply specified cell processing to different search results
        #region Cell Processing Components
        /// <summary>
        /// Apply a process to all matching cells. Takes a function returning void.
        /// </summary>
        /// <param name="criteriaFunctor">Func<List<SheetCell>></param>
        /// <param name="cellFunctor">Action<SheetCell></param>
        public static void ProcessMatchingCells
        (
            Func<List<SheetCell>> criteriaFunctor,
            Action<SheetCell> cellFunctor
        )
        {
			try
			{
				foreach (SheetCell cell in criteriaFunctor())
                {
                    cellFunctor(cell);
                }
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);

                throw;
            }
        }

        /// <summary>
        /// Apply a process to all matching cells. Takes a function returning Boolean.
        /// </summary>
        /// <param name="criteriaFunctor">Func<List<SheetCell>></param>
        /// <param name="cellFunctor">Func<SheetCell, bool></param>
        /// <returns>bool</returns>
        public static bool ProcessMatchingCells
        (
            Func<List<SheetCell>> criteriaFunctor,
            Func<SheetCell, bool> cellFunctor
        )
        {
            bool returnValue = default;
			try
			{
				foreach (SheetCell cell in criteriaFunctor())
                {
                    returnValue = cellFunctor(cell);
                }
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);

                throw;
            }
            return returnValue;
        }
        #endregion Cell Processing Components
        #endregion Static Methods

        #region layout examples
        /*
         *   0   1   2   3   
         *  -----------------
         * 0|Q1 |Q2 |Q3 |Q4 |     Quarter
         *  -----------------
         *  (null)
        */
        /*
         *   0     1   2   3   4   
         *  -----------------------
         * 0|#####|Q1 |Q2 |Q3 |Q4 |     Quarter
         *  -----------------------
         * 1|Belts|100|110|120|130|
         *  -----------------------
         * 2|Hats |180|190|200|210|
         *  -----------------------
         *  Department
        */
        /*
         *   0     1   2   3   4   5   6   7   8   
         *  --------------------------------------- 
         * 0|#####|2001           |2002           |     Year
         *  ---------------------------------------  
         * 1|#####|Q1 |Q2 |Q3 |Q4 |Q1 |Q2 |Q3 |Q4 |     Quarter
         *  ---------------------------------------  
         * 2|Belts|100|110|120|130|140|150|160|170| 
         *  ---------------------------------------  
         * 3|Hats |180|190|200|210|220|230|240|250| 
         *  --------------------------------------- 
         *  Department
        */
        /*
         *   0     1     2    3    4    5    6    7    8    9
         *  -----------------------------------------------------  
         * 0|#####|#####|2001               |2002               |     Year
         *  -----------------------------------------------------  
         * 1|#####|#####|Q1  |Q2  |Q3  |Q4  |Q1  |Q2  |Q3  |Q4  |     Quarter
         *  -----------------------------------------------------  
         * 2|Belts|Units|100 |110 |120 |130 |140 |150 |160 |170 | 
         *  -     -----------------------------------------------  
         * 3|     |Price|1.00|1.10|1.20|1.30|1.40|1.50|1.60|1.70| 
         *  -----------------------------------------------------  
         * 4|Hats |Units|180 |190 |200 |210 |220 |230 |240 |250 | 
         *  -     -----------------------------------------------  
         * 5|     |Price|1.80|1.90|2.00|2.10|2.20|2.30|2.40|2.50| 
         *  -----------------------------------------------------  
         *  Department, Details
        */
        #endregion layout examples
    }
}
