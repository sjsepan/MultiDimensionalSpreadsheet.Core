﻿using System;
using System.ComponentModel;
using System.Runtime.Serialization;
using System.Reflection;
using Ssepan.Application.Core;

using Ssepan.Utility.Core;

namespace MultiDimensionalSpreadsheetLibrary.Core
{
	/// <summary>
	/// CategoryItem
	/// </summary>
	//[Serializable()]
	[DataContract(IsReference = true)]
    [TypeConverter(typeof(ExpandableObjectConverter))]
    public class CategoryItem :
        SettingsComponentBase,
        IEquatable<CategoryItem>,
        INotifyPropertyChanged
    {
        #region Declarations
        public const string NewItemName = "(new item)";
        #endregion Declarations

        #region Constructors
        static CategoryItem()
        {
        }

        public CategoryItem()
        {
        }

        /// <summary>
        /// Parent name is the category name.
        /// </summary>
        /// <param name="name">string</param>
        public CategoryItem(string name) :
            this()
        {
            try
            {
                Name = name;
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);

                throw;
            }
        }

        /// <summary>
        /// Parent  is the category .
        /// </summary>
        /// <param name="name">string</param>
        /// <param name="parent">Category</param>
        public CategoryItem(string name, Category parent) :
            this(name)
        {
            try
            {
                Parent = parent;
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);

                throw;
            }
        }
        #endregion Constructors

        #region INotifyPropertyChanged support
        new public event PropertyChangedEventHandler PropertyChanged;

        new void OnPropertyChanged(string propertyName)
        {
            try
            {
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);

                //throw;
            }
        }
        #endregion INotifyPropertyChanged support

        #region IEquatable<T>
        /// <summary>
        /// Compare property values of this object to another.
        /// </summary>
        /// <param name="other">CategoryItem</param>
        /// <returns>bool</returns>
        public bool Equals(CategoryItem other)
        {
			bool returnValue;
			try
            {
                if (this == other)
                {
                    returnValue = true;
                }
				else if (Name != other.Name)
				{
					returnValue = false;
				}
				else if (Parent != other.Parent)
				{
					returnValue = false;
				}
				else
				{
					returnValue = true;
				}
			}
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);

                throw;
            }

            return returnValue;
        }

        #endregion IEquatable<T>

        #region Properties

        #region Persisted Properties
        private string _Name;
        [DataMember]
        public string Name
        {
            get { return _Name; }
            set
            {
                _Name = value;
                OnPropertyChanged(nameof(Name));
            }
        }

        private Category _Parent;
        [DataMember]
        public Category Parent
        {
            get { return _Parent; }
            set
            {
                _Parent = value;
                OnPropertyChanged(nameof(Parent));
            }
        }

        //private Category _Parent = default(Category);
        //[DataMember]
        //public Category Parent
        //{
        //    get { return _Parent; }
        //    set { _Parent = value; }
        //}
        #endregion Persisted Properties
        #endregion Properties
    }
}
