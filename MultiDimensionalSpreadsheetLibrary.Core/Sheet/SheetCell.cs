﻿using System;
using System.ComponentModel;
//using System.Linq;
using System.Runtime.Serialization;
using System.Xml.Serialization;
using System.Reflection;
using Ssepan.Application.Core;

using Ssepan.Collections.Core;
using Ssepan.Utility.Core;

namespace MultiDimensionalSpreadsheetLibrary.Core
{
	/// <summary>
	/// SheetCell
	/// </summary>
	//[Serializable()]
	[DataContract(IsReference = true)]
    [TypeConverter(typeof(ExpandableObjectConverter))]
    public class SheetCell :
        SettingsComponentBase,
        IDisposable,
        IEquatable<SheetCell>,
        INotifyPropertyChanged
    {
        #region Declarations
        new private bool disposed;
        #endregion Declarations

        #region Constructors
        static SheetCell()
        {
        }

        public SheetCell()
        {
            try
            {
                if (CategoryItems != null)
                {
                    CategoryItems.ListChanged += CategoryItems_ListChanged;
                }
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
                throw;
            }
        }

        public SheetCell(string value)
        {
            try
            {
                Value = value;

                if (CategoryItems != null)
                {
                    CategoryItems.ListChanged += CategoryItems_ListChanged;
                }
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);

                throw;
            }
        }

        public SheetCell(string value, EquatableBindingList<CategoryItem> categoryItems)
        {
            try
            {
                Value = value;
                CategoryItems = categoryItems;

                if (CategoryItems != null)
                {
                    CategoryItems.ListChanged += CategoryItems_ListChanged;
                }
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);

                throw;
            }
        }
        #endregion Constructors

        #region IDisposable 
        ~SheetCell()
        {
            Dispose(false);
        }

        new public void Dispose()
        {
            // dispose of the managed and unmanaged resources
            Dispose(true);

            // tell the GC that the Finalize process no longer needs
            // to be run for this object.
            GC.SuppressFinalize(this);
        }

        new protected virtual void Dispose(bool disposeManagedResources)
        {
            // process only if managed and unmanaged resources have
            // not been disposed of.
            if (!disposed)
            {
                //Resources not disposed
                if (disposeManagedResources)
                {
                    // dispose managed resources
                    if (CategoryItems != null)
                    {
                        CategoryItems.ListChanged -= CategoryItems_ListChanged;
                    }
                }
                // dispose unmanaged resources
                disposed = true;
            }
            else
            {
                //Resources already disposed
            }
        }
        #endregion IDisposable

        #region INotifyPropertyChanged support
        new public event PropertyChangedEventHandler PropertyChanged;

        new void OnPropertyChanged(string propertyName)
        {
            try
            {
				PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
			}
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);

                //throw;
            }
        }
        #endregion INotifyPropertyChanged support

        #region IEquatable<T>
        /// <summary>
        /// Compare property values of this object to another.
        /// </summary>
        /// <param name="other">SheetCell</param>
        /// <returns>bool</returns>
        public bool Equals(SheetCell other)
        {
			bool returnValue;
			try
            {
                if (this == other)
                {
                    returnValue = true;
                }
				else if (Value != other.Value)
				{
					returnValue = false;
				}
				else if (!CategoryItems.Equals(other.CategoryItems))
				{
					returnValue = false;
				}
				else
				{
					returnValue = true;
				}
			}
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);

                throw;
            }

            return returnValue;
        }

        #endregion IEquatable<T>

        #region ListChanged handlers
        void CategoryItems_ListChanged(object sender, ListChangedEventArgs e)
        {
            try
            {
                OnPropertyChanged(string.Format("Sheet[].Cells[].CategoryItems[{0}].{1}", e.NewIndex, e.PropertyDescriptor == null ? string.Empty : e.PropertyDescriptor.Name));
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);

                throw;
            }
        }

		#endregion ListChanged handlers
		#region Properties
		#region NonPersisted Properties
		/// <summary>
		/// In specific instances, allow notification to be suppressed when changing value.
		/// </summary>
		[XmlIgnore]
		public bool ValueChangingProgrammatically { get; set; }
		#endregion NonPersisted Properties

		#region Persisted Properties
		private string _Value;
        [DataMember]
        public string Value
        {
            get { return _Value; }
            set
            {
                _Value = value;
                if (!ValueChangingProgrammatically)
                {
                    OnPropertyChanged(nameof(Value));
                }
            }
        }

        private EquatableBindingList<CategoryItem> _CategoryItems = [];
        [DataMember]
        public EquatableBindingList<CategoryItem> CategoryItems
        {
            get { return _CategoryItems; }
            set
            {
                if (CategoryItems != null)
                {
                    CategoryItems.ListChanged -= CategoryItems_ListChanged;
                    //Note: these category items should already have a parent, since they *should be* coming from  items collection in category
                }
                _CategoryItems = value;
                if (CategoryItems != null)
                {
                    CategoryItems.ListChanged += CategoryItems_ListChanged;
                }
                OnPropertyChanged("Categories");
            }
        }

        //private EquatableBindingList<Category> _Categories = new EquatableBindingList<Category>();
        //[DataMember]
        //public EquatableBindingList<Category> Categories
        //{
        //    get { return _Categories; }
        //    set
        //    {
        //        if (Categories != null)
        //        {
        //            Categories.ListChanged -= new ListChangedEventHandler(Categories_ListChanged);
        //        }
        //        _Categories = value;
        //        if (Categories != null)
        //        {
        //            Categories.ListChanged += new ListChangedEventHandler(Categories_ListChanged);
        //        }
        //        OnPropertyChanged("Categories");
        //    }
        //}
        #endregion Persisted Properties
        #endregion Properties

    }
}
