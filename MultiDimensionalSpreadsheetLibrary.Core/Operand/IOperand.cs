﻿namespace MultiDimensionalSpreadsheetLibrary.Core
{
	/// <summary>
	/// Defines values to be operated upon.
	/// </summary>
	public interface IOperand
    {
		float Evaluate(FormulaExecutionContext context);
    }
}
