﻿using System;
using System.Collections.Generic;
using System.Linq;
using Ssepan.Collections.Core;
using Ssepan.Utility.Core;
using System.Reflection;

namespace MultiDimensionalSpreadsheetLibrary.Core
{
	public class OperandReference:
        OperandBase
    {
        #region Constructors
        public OperandReference()
        {
            try
            {
                Type = OperandType.CellReference;
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
                throw;
            }
        }

        public OperandReference(List<CategoryItem> value)
        {
            try
            {
                Type = OperandType.CellReference;
                _Value = value;
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
                throw;
            }
        }
        #endregion Constructors

        #region Properties
        private readonly List<CategoryItem> _Value;
        #endregion Properties

        #region IOperand Members
        /// <summary>
        /// Evaluate the operand. Must process the address reference and find a single sheet cell.
        /// </summary>
        /// <param name="context">FormulaExecutionContext</param>
        /// <returns>float</returns>
        public override float Evaluate(FormulaExecutionContext context)
        {
			float returnValue = default;
            List<CategoryItem> assignerCellCriteria = default;
            List<SheetCell> formulaSearchResults = default;
            Sheet sheet = default;

            try
            {
                //locate parent sheet, containing cells
                sheet = context.ExecutingFormula.Parent;
                if (sheet == null)
                {
                    throw new ApplicationException(string.Format("Unable to find parent sheet for formula: '{0}'", context.ExecutingFormula.Value));
                }

                //select all criteria in assignee cell EXCEPT those categories defined for assigner in the formula criteria.
                assignerCellCriteria = context.AssigneeCell.CategoryItems.Except(_Value, new EqualityComparerOfT<CategoryItem>((x, y) => x.Parent == y.Parent)).ToList();
                //add formula criteria to produce a complete set of criteria for an assigner cell.
                assignerCellCriteria.AddRange(_Value);

                //run assigner criteria
                formulaSearchResults = Sheet.GetCellsWhereAllSearchCategoryItemsMatchAnyInGivenCell([.. sheet.Cells], assignerCellCriteria);
                if ((formulaSearchResults == null) || (formulaSearchResults.Count == 0))
                {
                    throw new ApplicationException(string.Format("Unable to find assigner cell for parent sheet for formula '{0}'", context.ExecutingFormula.Value));
                }
                if (formulaSearchResults.Count > 1)
                {
                    throw new ApplicationException(string.Format("Too many results found for assigner cell for parent sheet for formula '{0}'", context.ExecutingFormula.Value));
                }

                //retrieve value from single cell in results
                returnValue = float.Parse(formulaSearchResults[0].Value);
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);

                throw;
            }
            return returnValue;
        }
        #endregion IOperand Members
    }
}
