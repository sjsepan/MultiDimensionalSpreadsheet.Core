﻿using System;
using Ssepan.Utility.Core;
using System.Reflection;

namespace MultiDimensionalSpreadsheetLibrary.Core
{
	public class OperandOperation:
        OperandBase
    {
        #region Constructors
        public OperandOperation()
        {
            try
            {
                Type = OperandType.Operation;
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
                throw;
            }
        }

        public OperandOperation(OperationBase value)
        {
            try
            {
                Type = OperandType.Operation;
                _Value = value;
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);

                throw;
            }
        }
        #endregion Constructors

        #region Properties
        private readonly OperationBase _Value;
        #endregion Properties

        #region IOperand Members
        /// <summary>
        /// Evaluate the operand. Must run the operation, which must evaluate the child operands.
        /// Must pass down context for any children that must do cell lookups.
        /// </summary>
        /// <param name="context">FormulaExecutionContext</param>
        /// <returns>float</returns>
        public override float Evaluate(FormulaExecutionContext context)
        {
			float returnValue;
			try
            {
                returnValue = _Value.Run(context);
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);

                throw;
            }
            return returnValue;
        }
        #endregion IOperand Members
    }
}
