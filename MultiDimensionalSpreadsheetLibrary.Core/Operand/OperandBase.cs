﻿namespace MultiDimensionalSpreadsheetLibrary.Core
{
	public abstract class OperandBase :
        IOperand
    {
        #region Declarations
        public enum OperandType
        {
            Literal,
            CellReference,
            Operation
        }

		#endregion Declarations
		#region Properties
		public OperandType Type { get; set; }
		#endregion Properties

		#region IOperand Members
		public abstract float Evaluate(FormulaExecutionContext context);
        #endregion IOperand Members
    }
}
