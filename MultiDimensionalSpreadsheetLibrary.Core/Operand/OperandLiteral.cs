﻿using System;
using Ssepan.Utility.Core;
using System.Reflection;

namespace MultiDimensionalSpreadsheetLibrary.Core
{
	public class OperandLiteral:
        OperandBase
    {
        #region Constructors
        public OperandLiteral()
        {
            try
            {
                Type = OperandType.Literal;
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
                throw;
            }
        }

        public OperandLiteral(float value)
        {
            try
            {
                Type = OperandType.Literal;
                _Value = value;
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);
                throw;
            }
        }
        #endregion Constructors

        #region Properties
        private readonly float _Value;
        #endregion Properties

        #region IOperand Members
        /// <summary>
        /// Evaluate the operand. Must return the literal value.
        /// </summary>
        /// <param name="context">FormulaExecutionContext</param>
        /// <returns>float</returns>
        public override float Evaluate(FormulaExecutionContext context)
        {
			float returnValue;
			try
            {
                returnValue = _Value;
            }
            catch (Exception ex)
            {
                Log.Write(ex, MethodBase.GetCurrentMethod(), Log.EventLogEntryType_Error);

                throw;
            }
            return returnValue;
        }
        #endregion IOperand Members
    }
}
